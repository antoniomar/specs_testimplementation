package eu.specs.project.enforcement.broker;

import static org.junit.Assert.*;

import java.util.List;

import org.jclouds.openstack.neutron.v2.domain.Network;
import org.jclouds.openstack.neutron.v2.domain.Subnet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import eu.specs.project.enforcement.broker.entities.BrokerTemplateBean;
import eu.specs.project.enforcement.broker.entities.InstanceDescriptor;
import eu.specs.project.enforcement.broker.entities.NodesInfo;
import eu.specs.project.enforcement.broker.entities.credentials.NodeCredential;
import eu.specs.project.enforcement.broker.entities.credentials.ProviderCredential;

public class CloudServiceComputeOpenStackTest {
	
	private static String publickey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCoTq6cH2zI2Fc8OHZUbx1AUlTcawp96Yuwa2ilxoAcdvWBePQk/dDkz6pkhmrLR0EM6Z44CGlGJiU9FBZUKgwuBuXnZ9iFGIl+xZ515KW0qn5FORqNEcEHiROKZj/tWRKmCTI7FyQgbRDSZcrKpKxtQE2JBKkznKwtS4bo2haKM8274Ebra1pC0Z3xBgcbwA0qGMIssWKDABeU4sSxOsCibXJRfhTKoVmokfnBNIBQ4kraYQHIOCNzTIvqKo/wZPkVgojOW+bEti2HNu5dvCcr0QoVAbnITJDmi8BM5td9qw6s3ran4Usoi75TQuobb7DMvZk1rIRmKRPjUKdP/nz7C4sR5zT1uDn2w3mTN3o0emrLC1J9LIA7F1RJJtmkscb6nNHzUfvZ2Fg5FjvGnldqO32Tilo+xczvpQIaQ9DyF25WmW4mCa4+DKAxZ0g4aRNHmUHSfsXdhxkvTA8BycnZauUfV1MO/HRSLfP6noA2bAfx9lRRn0JVw4GDiufHrJGRU0VJqi0KrYavIHAQbSvQ3IKlpybbl+5hyo1DnOxvhGWsxPLYrq2cN8a+H55ZZB/9gWqNd8Bj+P7CqsFmFiE5jxNV5WFSVrCwEVwbxr5yVmvKCP9B0NAiRUnub7boo20nTy3dGlf43knHMtVr1Ltky5Tdb7i4OMNBi9KsBPIxmw== your_email@example.com";
	private static String privatekey = "-----BEGIN RSA PRIVATE KEY-----\n" + 
			"MIIJKQIBAAKCAgEAqE6unB9syNhXPDh2VG8dQFJU3GsKfemLsGtopcaAHHb1gXj0\n" + 
			"JP3Q5M+qZIZqy0dBDOmeOAhpRiYlPRQWVCoMLgbl52fYhRiJfsWedeSltKp+RTka\n" + 
			"jRHBB4kTimY/7VkSpgkyOxckIG0Q0mXKyqSsbUBNiQSpM5ysLUuG6NoWijPNu+BG\n" + 
			"62taQtGd8QYHG8ANKhjCLLFigwAXlOLEsTrAom1yUX4UyqFZqJH5wTSAUOJK2mEB\n" + 
			"yDgjc0yL6iqP8GT5FYKIzlvmxLYthzbuXbwnK9EKFQG5yEyQ5ovATObXfasOrN62\n" + 
			"p+FLKIu+U0LqG2+wzL2ZNayEZikT41CnT/58+wuLEec09bg59sN5kzd6NHpqywtS\n" + 
			"fSyAOxdUSSbZpLHG+pzR81H72dhYORY7xp5Xajt9k4paPsXM76UCGkPQ8hduVplu\n" + 
			"JgmuPgygMWdIOGkTR5lB0n7F3YcZL0wPAcnJ2WrlH1dTDvx0Ui3z+p6ANmwH8fZU\n" + 
			"UZ9CVcOBg4rnx6yRkVNFSaotCq2GryBwEG0r0NyCpacm25fuYcqNQ5zsb4RlrMTy\n" + 
			"2K6tnDfGvh+eWWQf/YFqjXfAY/j+wqrBZhYhOY8TVeVhUlawsBFcG8a+clZrygj/\n" + 
			"QdDQIkVJ7m+26KNtJ08t3RpX+N5JxzLVa9S7ZMuU3W+4uDjDQYvSrATyMZsCAwEA\n" + 
			"AQKCAgEApzNTP4Hwfi59qzOkSgkAlC+2Yipsh92FikOJ+HYUOzLqfi78y+ripQyw\n" + 
			"h6EJYtxsgTlsltm0S54FjIHyFLaY1vi/WH3PmZlgrnIAE7qL68kcHAS4ncAvdR72\n" + 
			"LJ2bkFzWq9+i6RT6LMRs27eIGHwzV210GVRqCv3wBidr5NMTZURFdAlAKwvA5XR8\n" + 
			"ZT7JvQWzjkh+TgwCg4WJ57zIDhEB4dq8oLySRyK3gUFTTl6rwq6sEfog3Rj4DsjS\n" + 
			"jlSuHfERig7dfbPKs00QOa2Wa8RJXbwTOf824wc9Q+s9pKgHF67hq1atOmYUVjlN\n" + 
			"YEWvcIYqq5sHI005UowoV8B+FngJC3fvy8PZf2UKGi7Y8otrKJSaRgWe/8xGNFfS\n" + 
			"KkfkyY1+Bgh+QtoKiUFKxR7ejZBUpVYzxgE7SrupXv6hUnJ75G9XSjhvzvnNzQ8/\n" + 
			"NIAJ/rfLR9rq2XNVrHlBIQULOzDWneQH9QbXBIAGTNjv9hAYgZ0pd9PAmc1W2nNn\n" + 
			"hGpvENpRHI/pEU0GIZRRRijsehx3IpvJTMUf1g0t74xfp/RhRq8Ghp0iZNDiDyVd\n" + 
			"htWfTlw4F6tfYymt05MWX8QJHZfcw4kLLyCe4i6vnMAAgdTW3jPbRwVTWfhQIbIl\n" + 
			"DnKslWNA1OA47yWF5f3+cVLY2HDBmREOV1v9ViycnEsEZtCW90ECggEBANmhW68h\n" + 
			"vaVmUTP6a88eUeL4FwAtUaULbdMb8o3dXvA6/Ty6PsmIwFdRMlax7Q6VS1mmD/iQ\n" + 
			"z/Ckv3hIB6STFBenoHKHzQyiQDyh1OuVWdjkilKq2gY4jvOUm3m/m9rOXVah78nr\n" + 
			"jfSJ6+u3I2XcUx11bzul//jmj3idAFaQoraXz5cpsBKVGM3psd9sQoZg2c+qvVy6\n" + 
			"vQwaankAz7Rl0pIvQBVBl2YSMWjFnpqd/TMFwCOKQqF1Ra+STHxf2rwrjEzB/thE\n" + 
			"UwndTZEMv0JT5E29sgokFBBS6Arh0q/er4yRAF12HlcM3no6avfPBgtwCVKpZApc\n" + 
			"F7OiS7ppA0JNqhECggEBAMX7J9E9OWJ99mpbpF3iltp4bQf40Z0nPmAuzHjrV9+z\n" + 
			"D3n65Ms12VNmXHfh0OJMjb0y2slHWFx1WR+BjJpTRQeaIbXWaM4AMSEkZqF/3iOu\n" + 
			"fjZ3LrV4gdSubdhZSlCFgzGQjnQCzo0VR2kvc7t5E7ArXHs3SinX+R4BJXr2SPsY\n" + 
			"ZFAqqa6HCN3JEM+qx1TQhU7e53yl4GJmZIqzPr0ulALnQA8SdJmGdZOqyEtXXls2\n" + 
			"hPqypaG72GEWQok5QAp70zA4OLBPGqk2xgHVqkWsd7RzIhUyVNh9cJNq3coKUSt4\n" + 
			"jYT1QLUaOMUyFTx8ry76HS+k/JVimGGCWH2GDqDB1OsCggEADRLdcCLlwqbn3+h+\n" + 
			"S331y129+/u8+N475b+7DXyr9GCGz/wcQ30cmOI+1OhxZjvfv+3y7aq9YyZyfiZN\n" + 
			"lzxiq50dTWQNFGI2262ZHeHl+NqZ2Kd823zQUZ+uy1yQX0+gWdD2l7m32GXkr09c\n" + 
			"KAVZVN62JO4u92PaYJOF7q1XqQJrrRgLBMLCL7SHQ/W79ViiHRCTuZtaOfzoQoai\n" + 
			"86JeSPvfrKc5Eh+rqjCc+4rcKBGDXl8y64IhApAWWi5j9IPlNMM+rP1Fw8TwoPMU\n" + 
			"Fkb14xmvNV6/ec+RMv4n6vrErvwS+opa/j2+q7cQr/xWz58WYEnewaSQo/Du/a7L\n" + 
			"TFNgEQKCAQBY6IQmE/+MOpMJimQTAWnLbJ9SICxKDdCWMw4CE3Yf8HK3zbMniuQw\n" + 
			"7twBvjgnGEfiuk7EazTEOLKwUmZI+7IZSt9uoH3qXO2zO3yXdCvXDEqDvRwNxLNR\n" + 
			"Ro/vw5F6/ao//PHsKUKG2xJ5qySnASWVTLO4Zcj5Z5LpJYymXqnBkV+jHLWcuemC\n" + 
			"aAQTrlbbwNTM6Y89S+Sra94k53/6R0uSzjTXT+tq8pYknWJbaz3lamNnjgiRzCDM\n" + 
			"4Mk6hKo8ywS5N9ZFXxDTAtUDB+fyaBRNxhJ9QGAX5KVEno5BaEaxZe7v0Y+ghMkP\n" + 
			"zYD56+hkCVBIECiaEW/rAjUpEaXKDaHHAoIBAQCgrSmaB0sCYi5FDktL109V5pK9\n" + 
			"TS2mReMVWjmhQ1HD8C6V8+A9rs0yKfEMpgN0SneHAXVoHZtDoARrnAKSzzpoTAs8\n" + 
			"eL2WKcZJQzKp243lunHF6P1SJaM/lxqClStc/t9ctU9RH/j3J96cw6zpKqrgYZxd\n" + 
			"LvyfsN0LM7UlZmkdDiUi8rd4KmLcTqTyxqAjyKi4OpDCRLvOjHgLuDZXCHYVSaZS\n" + 
			"6WQF06xjyeASAAXmP/1IMghL7e/ayh4ZE0CVqozYvbt33F7y8Ci2tKPiGDfGt7+D\n" + 
			"R2f0P3WDjQmJ5/Rjqq4saQFZtP5mDUQRkt18XBiU/f3RzheBD5RDKeNnMewE\n" + 
			"-----END RSA PRIVATE KEY-----";
	
	private static CloudServiceComputeOpenStack CloudServiceComputeOpenStack; 
	private static BrokerTemplateBean bean = new BrokerTemplateBean();
	
	//########  Local OpenStack infrastructure
//	private static String username = "demo";
//	private static String tenant = "demo";	//project associated to the user
//	private static String password = "password";
//	private static String oppenstackAddress = "http://192.168.27.100:5000/v2.0/";
	
	//########  Public OpenStack infrastructure
	private static String username = "facebook10206354222007831";
	private static String tenant = "facebook10206354222007831";	//project associated to the user
	private static String password = "zLchYR3JLB09txAh";
	private static String oppenstackAddress = "http://128.136.179.2:5000/v2.0"; 	//TryStack
	
	private static String groupName = "testgroup";
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		//######### Local OpenStack infrastructure
		//Ubuntu
//		bean.appliance = "RegionOne/62e34830-0709-4d06-bb2d-fb15b9fe132e";
//		bean.hw = "RegionOne/2";
//		bean.zone = "RegionOne";
		
//		//cirros
//		bean.appliance = "RegionOne/5584961b-1a1c-4e9a-ac4b-304d21fc6643";
//		bean.hw = "RegionOne/1";
//		bean.zone = "RegionOne";
		
		
		//######### Remote TryStack infrastructure
		//OpenSuse13.2
		bean.appliance = "RegionOne/acd08d90-91a3-4488-9990-728f91eb0d95";
		bean.hw = "RegionOne/2";
		bean.zone = "RegionOne";
		
		//Ubuntu14.04
//		bean.appliance = "RegionOne/cb6b7936-d2c5-4901-8678-c88b3a6ed84c";
//		bean.hw = "RegionOne/2";
//		bean.zone = "RegionOne";
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCloudServiceComputeOpenStack() {
		
		ProviderCredential providerCredential = new ProviderCredential(tenant+":"+username, password);
		
		CloudServiceComputeOpenStack = new CloudServiceComputeOpenStack(oppenstackAddress, "root", providerCredential);

		//Descriptor of the machine to test
		InstanceDescriptor descr = new InstanceDescriptor(bean.appliance, 
				bean.zone,bean.hw);
		
		//password that we want to to create, public key to authenticate and private key to install on the host  
		NodeCredential nodeCredential = new NodeCredential("rootpassword", publickey.trim(), privatekey.trim());
		
		try {
			
			CloudServiceComputeOpenStack.clearGroupNodesAndNetwork("testrouter", "testNet", groupName);
			
			Network network = CloudServiceComputeOpenStack.createNetwork("testNet");
			
			Subnet subnet = CloudServiceComputeOpenStack.createSubnet(network.getId(),"10.0.0.0/8");
			
			CloudServiceComputeOpenStack.connectNetworkToExtern("testrouter", subnet.getId());
			
			boolean resultCreateGroup = CloudServiceComputeOpenStack.createSecurityGroup(groupName, "this is a group generated for testing");
			
			NodesInfo nodesInfo = CloudServiceComputeOpenStack.createNodesInGroup(groupName,
					1//number of instances
					, descr, 
					nodeCredential, publickey, network.getId(), 
					22,80,11211, 9390,1514);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		fail("Not yet implemented");
	}

	@Test
	public void testCreateNodesInGroup() {
		fail("Not yet implemented");
	}

	@Test
	public void testDestroyClusterWithName() {
		fail("Not yet implemented");
	}

	@Test
	public void testAssociateFloatingIP() {
		fail("Not yet implemented");
	}

	@Test
	public void testAssociatePublicIP() {
		fail("Not yet implemented");
	}

	@Test
	public void testCreateNetwork() {
		fail("Not yet implemented");
	}

	@Test
	public void testCreateSubnet() {
		fail("Not yet implemented");
	}

	@Test
	public void testCreateSecurityGroup() {
		fail("Not yet implemented");
	}

}
