package eu.specs.project.enforcement.broker;

import static org.junit.Assert.*;

import java.util.NoSuchElementException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import eu.specs.project.enforcement.broker.entities.BrokerTemplateBean;
import eu.specs.project.enforcement.broker.entities.InstanceDescriptor;
import eu.specs.project.enforcement.broker.entities.NodesInfo;
import eu.specs.project.enforcement.broker.entities.credentials.NodeCredential;
import eu.specs.project.enforcement.broker.entities.credentials.ProviderCredential;

public class CloudServiceTest {
	private static String publickey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCgCxJc/vA19ZxwTl/PZdGGAWIdxL7u54vxa3J1hu7OcGumkqFbZPlMvkTZnBXzBXW/MDEkz0IeAFmvcfgqNUJu1tczEebYyTYyut8maAAcpDLNCZplQT6JVIAZ7l17dqRib51YIgT8bm/c2NLYWmCFWrHOB5AHzotOfEVgLgwUZAYu9W1SK0pSx+oR12ejf7BnB6Qz9hffM4Sf4WKdoO1t9l41x+3fUV89NRU+bvgnnvWgqEyKn6LFng0KU+mlcPWRaYuOMdOMsxG0D6eSAfepcjp2W9mCWQQCIoOz7wbPZo4a1NyljXJ7rnuCQs3EM1cFQWnwy1IX7Osk9h5Opo3rDzzgTrIfz0QEoyjnU0hDhdauvxrfwG1hpzM3yypEJc4JJRxIsF9LPFzg5Msgp5K7khsj7LVJNY0LHrhkelH7F5qnxvfXEcFfZBNb1WyVwbUetVf5fvnm3qDF5wg6xkmlyYNy0c0Ra/iC+05U9yqD4yBZIYu9VXcHhs7kkUJjvfc2fr00YRS/iXaOoLGd3s5QhKUn6d/vQAIqinUytbePwZ+E+EFrjsHaqXJxch2rzdaqU+Opaac9MmgGlDHtDHp71VV1IIF1PdI3vZBrbnfS62ZQR+Y1eMBv7x64NxF11jCmF53UgJUnZPNwNZS5mQWNrjSUMcPgH+X497jBwHWDxw== your_email@example.com";
	private static String privatekey = "-----BEGIN RSA PRIVATE KEY-----\n" + 
			"Proc-Type: 4,ENCRYPTED\n" + 
			"DEK-Info: AES-128-CBC,64BCC54A21CFB214BF1D721168335160\n" + 
			"\n" + 
			"xXi5dIC0IhpCJkEXVoYqHtc6uWgWgz1mmL3+8kwTQqVxGVyPMQLAvtfPtaZv1LMA\n" + 
			"yhbHztN/I5waZA4aQBNoMqnf8xY59JjDle2ZAZm4tvKUoDxnw7Wk2cECDQXAk8rj\n" + 
			"VK/KRLWQx7CJ6Wthw4eeV551/J244P+da/ehuWd+0pyMiXh/fpy2+FW29XNkY+Nf\n" + 
			"0E+SohI4dvYXEPA5yUlaIsEU3PKw10ajDZXRBuKdZI9z/HYXTsSSlYiWodLFkfHN\n" + 
			"GYozef5henxgKzehuZ+g2ehaeoSoFiTRJl4Xr5ioehEJWRvo7kyFD0lR+jxhs3Vk\n" + 
			"c5ramFHZdt07xjuI1CNJNfAmSqcn8bNuv2xW8IIaflveEbETKMakbLmT3vylImdZ\n" + 
			"S0fEcebZyW5jm+B9Nw0ykrpvYlEKErOLhEy8mBj9T5akWr7MxYQJdw9DrXZsp6mO\n" + 
			"vK7/AMgn9r3mNwbyStt58h4B5QW25OzQSp+AzcPLCaQIRBXk6PJ6vLMqw7/A51y7\n" + 
			"kbIW5cK28hkRDhj8GCQUDVnjimmyuZV5yl8HpMKOA02ix2jeuLV7H3RsCPGpDrOl\n" + 
			"78oG5i5hG0UMNWI2FfKTlh05gZ/EdSg1UfJkNSvgtqjBN0+AugNPfc3MrHd+AqeB\n" + 
			"BFXjJPqOheoMV4f05a40/vD1VMj+dt2Y6KA/97LAOKvGJq+ODvLYTaulcdYnobS/\n" + 
			"WWFyhGnEpbwt8ersHPZzTCe/ixwk2TkfEA3NeIwXe1tYK/f3y5HvbrjTA2JgqLdn\n" + 
			"/9GYx7F0wim1vQWd3RlhJ9sgKi2ViK1uZjv+nDikEPKIyRqJlzOo14oH47WisnrK\n" + 
			"isOLRK47+HMgswCcxVnfbTnSIcYmPH03ElMOGB0CLK3j7xM4FNIHyXddXcKyx5WZ\n" + 
			"uEhncd5TJcKZIriOFUpc2ygB8p0XPvA2O63Bg0igFApV7T8/z8/Uey8McwReWl1y\n" + 
			"DBeEAKEc800V2s20Ga8ypsSKBunaF5pa37BSAykokcHN60jKzpo9vd5nOJeNn7ng\n" + 
			"McZxpXShpoXCl1ZFwADMkmbNWkIEkWiSbhiVbqUeAW6p606aUwJAHUbIkZDugkxc\n" + 
			"Sp5Ika6keey7Lb40zQAA5JhCRW7SfCQIxjqkYwf0+4BoALeCpkJMtdmALVf+edY6\n" + 
			"WUjgOrwKNuBAuRDPxpKZeCHszCoyUJg7GiydJwlpBcw7WkwbgbteOjrSimLhRmGa\n" + 
			"lSTw0hR+wCaT7FdBGeMAP3KCmimhWuVhhyqK/+LfuziixPddXeDtgc1r/ZI1R9Cf\n" + 
			"z1CZfkhaXVxO+l4dqmMZpbZDLC2+TF53tdqNs+X6mpsTZ3lqgT5j1D7T8s1uw0rs\n" + 
			"5QkemHfLuMU3ZqO/IbeX45Da8ydQV+sVgmu+6mtKLNU6eLp8nLjISMD10VMvqxow\n" + 
			"WQgjpUKtRJBYpA2NtezNeLBjHN2SoUqzsN5ObCQb20vtvExWBM05eqgTW2R0a+T+\n" + 
			"KHV54+Ds57Ee+kdSRXoHLFnkGN7PUB1lGCUrUrXD+Zs7bAKcYMYbHH9eV6WwYKRd\n" + 
			"8KbuS4Pyrt0kwa+oHLRub5Xkx5SeM/e+zOYbAOO1hHbdg+mekySZnruNOfpUEF0S\n" + 
			"hNOD6+4FRry8MUg2otrwYD6L/G+SCRdCR5Y+1dRdOmXr8mUfFjVg938pG5lX2P1G\n" + 
			"nakqM9qkmI6ulklmHIgTz2eH1k2rfy29WKXE6w4Cwv/3ENlV8vaFhp2LMkyL8ikE\n" + 
			"zzc2iPzk7qDyRziHMJmKrRfDTcpkrt00FVZmPeYevk9bziZY6M0QdiuiifIPH6sA\n" + 
			"88grerD1rRJxce6bvJKKL3SG7tU4DBG0m3ZAC6ORJoZQAVfrO+Dl3M0b2DR+g5xR\n" + 
			"XXqa09dFWJdjoKO55bLMKT52l8U1X+bvk+NEMNL/jRnVloB5ogfcTr+xuVLFQPd6\n" + 
			"yKITxk7MlkSpDThW2f2FBA26XfVg7hK+POL2qyYOaZ5WQkIyP8lRfMyoInDvIFc5\n" + 
			"pBDB44shTgZNU0/C4GwhZ4jumY4ClaQ1HOK/iR2NmyxYIf/VwKjYPHIizPayTKEM\n" + 
			"XylDHX36lhNGv/2hsyCEbhPxnpGwEjZnd8hiiU2W2tZDKuxV/zmWxBm7DHPeZ3tj\n" + 
			"Q7At6pgI1+mzwsowTiQ7u8AnIGeU3tr9WGE+4c/+meIS3z21QOlah/bbG16EwVoI\n" + 
			"NxYbyFzw34l32hq/7UR2n3e/RQG6twGQp21tLMVuJtt5QXkYYU7nAsEEvdiU0GE8\n" + 
			"n/vvoQEkUHMW1MV7Bks3qnkD0OgA9MIAxM2hviD18uoXensG0gDOBrPcFe6zXtIX\n" + 
			"5z3djp5D0Pr02cUleHOAlAXFW9AZIDUfpbv1J/mGHY0Q5EQv8t1y/lCC41KuAD/C\n" + 
			"i1zdA/ko3TYxZWyIEIryX3I7RBYIwUHLgj1cCJCKUFeIIcsOUJKJKBc1Pn5IuV/r\n" + 
			"JesEqgs8I5jCXnTEdItId+u7G1nZ78373YNGy8Vo3dlKcM9pGBVoZikb+mt7wtk8\n" + 
			"a96TfP3orpdsN+23ssR/czxr8jUV+Wn/vqAx5Tdw8SqvnqMAKAcUfF65Fqpzij39\n" + 
			"c14wylh4/baJhMkAU1IRXkhojAxt7UxhuqtdwSZYHaL+0rOpJzRRiu7iylnQYF7y\n" + 
			"zdxXwQwPYgFs4gvx0eqpagBIfaWT6Ufo9rRVnxlT7PWaP7SSsxg/irpi7vi+0bTQ\n" + 
			"LcrD1qvO/InJBeTxxaKNQ+EWP18rcwgyUuytKeeQJCzLB7eeLkqM3vVDKLyfiOeS\n" + 
			"pdmjTIHg0fMrOgl9QZc1TefrH6OaNZgTJ5CJjAAzVO4fkL59G1uI4n0Nn32x4d7A\n" + 
			"jyAXkMU13tMO9o7aVH6KQXlz7NX7bNeu9L0uC6PwHBLs3zjfbrbExLeL06k5Dx1M\n" + 
			"SM2nSLgh8jA38txt8dgNrT1q6RGR+fcUCPjrGGQ0A6+nl6m81uMLTYAidN+WOgSU\n" + 
			"aeQx5ef9ZklR8zV0kDOzvSGF7Gd6GrZcUunqSHUhxgPjsXj/QSHA2GkGB2cDrkFP\n" + 
			"ny92gnys4Miq9OcteNn0xGsfi1xMQFxOzyfAGR1yzNshlNJ+XzftdzX3ojV6Xlcd\n" + 
			"sZNQaYhO3iulF+hh2hoN2Es3XMwzgH8golxNbhti8MsAiPrc458H9U/HJsaItBdK\n" + 
			"-----END RSA PRIVATE KEY-----";
	
	private static CloudService cloudService; 
	private static BrokerTemplateBean bean = new BrokerTemplateBean();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		//cirros
		bean.appliance = "us-east-1/ami-ff0e0696";
		bean.hw = "t1.micro";
		bean.zone = "us-east-1";
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCloudService() {
		ProviderCredential providerCredential = new ProviderCredential("AKIAJMLKJU32LHT527GQ", "5+E0jJxwWihI+GQJM1NJ6BX00isVT9Lco/0aa0eB");
		
		cloudService = new CloudService("openstack-nova", "cirros", providerCredential);

//		//cirros test
//		InstanceDescriptor descr = new InstanceDescriptor("RegionOne/5584961b-1a1c-4e9a-ac4b-304d21fc6643", 
//				"RegionOne","RegionOne/1");
		//Ubuntu 14.04 test
		InstanceDescriptor descr = new InstanceDescriptor(bean.appliance, 
				bean.zone,bean.hw);
		
		NodeCredential nodeCredential = new NodeCredential("rootpassword", publickey, privatekey);
		
		try {
			NodesInfo nodesInfo = cloudService.createNodesInGroup("default",
					1//number of instances
					, descr, 
					nodeCredential, 22,80,11211, 9390,1514);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		fail("Not yet implemented");
	}

}
