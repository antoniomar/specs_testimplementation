package eu.specs.project.enforcement.broker;

import static org.junit.Assert.*;

import java.nio.charset.StandardCharsets;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import eu.specs.project.enforcement.broker.entities.credentials.ProviderCredential;

public class StorageServiceTest {

	private static String provider = "s3";
	private static String username = "AKIAJMLKJU32LHT527GQ";
	private static String password = "5+E0jJxwWihI+GQJM1NJ6BX00isVT9Lco/0aa0eB";
	private static ProviderCredential providerCredential;
	private static String containerName = "testcontainerspecs";
	private static String fileName = "testfilespecs";
	private static String directoryName = "testdirspecs";
	private static boolean versioning = true;
	private static boolean createUser = true;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		providerCredential = new ProviderCredential(username, password);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

//	@Test
//	public void testCreateContainerString() {
//		boolean result;
//		StorageService storageService = new StorageService(provider, providerCredential);
//		result = storageService.createContainer(containerName);
//		assertTrue("I must handle the exceptions", result);
//	}
//
//	@Test
//	public void testCreateContainerStringString() {
//		boolean result;
//		StorageService storageService = new StorageService(provider, providerCredential);
//		result = storageService.createContainer(containerName, "us-west-2");
//		assertTrue("I must handle the exceptions", result);
//	}

//	@Test
//	public void testCreateContainerStringStringBooleanBoolean() {
//		boolean result;
//		StorageService storageService = new StorageService(provider, providerCredential);
//		result = storageService.createContainer(containerName, "us-west-2", versioning, createUser);
//		assertTrue("I must handle the exceptions", result);
//	}
//
//	@Test
//	public void testCreateContainerStringStringStringBoolean() {
//		boolean result;
//		StorageService storageService = new StorageService(provider, providerCredential);
//		result = storageService.createContainer(containerName, "us-west-2", "us-west-1", createUser);
//		assertTrue("I must handle the exceptions", result);
//	}

	@Test
	public void testAllOperations() {
		boolean result;
		StorageService storageService = new StorageService(provider, providerCredential);
		//it the container already exist it is removed and regenerated
		result = storageService.createContainer(containerName, "us-west-2", "us-west-1", createUser);
		//generate and upload a file
		String testFileString = "test file to load on Amazon S3";
		byte[] dataToStore = testFileString.getBytes();
		storageService.uploadBlob(containerName, fileName, dataToStore );
		//create a directory and upload the same files into
		storageService.createDirectory(containerName, directoryName);
		storageService.uploadBlob("testttstst", "dirtest/testFile", dataToStore );
		//download a file from the container
		byte[] test = storageService.downloadBlob("testttstst", "testFile");
		String str = new String(test, StandardCharsets.UTF_8);
		System.out.println(str);
		storageService.deleteContainer("testttstst");
		
		assertTrue("I must handle the exceptions", result);
	}

	@Test
	public void testUploadBlob() {
		fail("Not yet implemented");
	}

	@Test
	public void testDownloadBlob() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteBlob() {
		fail("Not yet implemented");
	}

	@Test
	public void testCreateDirectory() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteDirectory() {
		fail("Not yet implemented");
	}

}
