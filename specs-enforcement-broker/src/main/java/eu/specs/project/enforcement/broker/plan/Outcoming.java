/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specs.project.enforcement.broker.plan;

import java.util.List;

public class Outcoming{
   	private String destination_ips;
   	private List destination_nodes;
   	private String networkInterface;
   	private String port_list;
   	private String proto;

 	public String getDestination_ips(){
		return this.destination_ips;
	}
	public void setDestination_ips(String destination_ips){
		this.destination_ips = destination_ips;
	}
 	public List getDestination_nodes(){
		return this.destination_nodes;
	}
	public void setDestination_nodes(List destination_nodes){
		this.destination_nodes = destination_nodes;
	}
 	public String getnetworkInterface(){
		return this.networkInterface;
	}
	public void setnetworkInterface(String networkInterface){
		this.networkInterface = networkInterface;
	}
 	public String getPort_list(){
		return this.port_list;
	}
	public void setPort_list(String port_list){
		this.port_list = port_list;
	}
 	public String getProto(){
		return this.proto;
	}
	public void setProto(String proto){
		this.proto = proto;
	}
}
