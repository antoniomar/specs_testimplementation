/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Implement the funcionalities to create and manage the nodes in a Cloud Service Provide
using the jcloud library

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specs.project.enforcement.broker;

import static com.google.common.base.Predicates.and;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.getOnlyElement;
import static org.jclouds.compute.config.ComputeServiceProperties.TIMEOUT_SCRIPT_COMPLETE;
import static org.jclouds.compute.options.TemplateOptions.Builder.overrideLoginCredentials;
import static org.jclouds.compute.predicates.NodePredicates.TERMINATED;
import static org.jclouds.compute.predicates.NodePredicates.inGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.jclouds.Constants;
import org.jclouds.ContextBuilder;
import org.jclouds.compute.ComputeService;
import org.jclouds.compute.ComputeServiceContext;
import org.jclouds.compute.RunNodesException;
import org.jclouds.compute.domain.ExecResponse;
import org.jclouds.compute.domain.Hardware;
import org.jclouds.compute.domain.Image;
import org.jclouds.compute.domain.NodeMetadata;
import org.jclouds.compute.domain.Template;
import org.jclouds.compute.domain.TemplateBuilder;
import org.jclouds.compute.predicates.NodePredicates;
import org.jclouds.domain.Location;
import org.jclouds.domain.LoginCredentials;
import org.jclouds.openstack.keystone.v2_0.config.CredentialTypes;
import org.jclouds.openstack.keystone.v2_0.config.KeystoneProperties;
import org.jclouds.openstack.neutron.v2.NeutronApi;
import org.jclouds.openstack.neutron.v2.domain.ExternalGatewayInfo;
import org.jclouds.openstack.neutron.v2.domain.FloatingIP;
import org.jclouds.openstack.neutron.v2.domain.Network;
import org.jclouds.openstack.neutron.v2.domain.Network.CreateNetwork;
import org.jclouds.openstack.neutron.v2.domain.Port;
import org.jclouds.openstack.neutron.v2.domain.Router;
import org.jclouds.openstack.neutron.v2.domain.Router.CreateRouter;
import org.jclouds.openstack.neutron.v2.domain.RouterInterface;
import org.jclouds.openstack.neutron.v2.domain.RuleDirection;
import org.jclouds.openstack.neutron.v2.domain.RuleEthertype;
import org.jclouds.openstack.neutron.v2.domain.RuleProtocol;
import org.jclouds.openstack.neutron.v2.domain.SecurityGroup;
import org.jclouds.openstack.neutron.v2.domain.Rule.CreateRule;
import org.jclouds.openstack.neutron.v2.domain.SecurityGroup.CreateSecurityGroup;
import org.jclouds.openstack.neutron.v2.domain.Subnet;
import org.jclouds.openstack.neutron.v2.domain.Subnet.CreateSubnet;
import org.jclouds.openstack.neutron.v2.extensions.FloatingIPApi;
import org.jclouds.openstack.neutron.v2.extensions.RouterApi;
import org.jclouds.openstack.neutron.v2.extensions.SecurityGroupApi;
import org.jclouds.openstack.neutron.v2.features.NetworkApi;
import org.jclouds.openstack.neutron.v2.features.PortApi;
import org.jclouds.openstack.neutron.v2.features.SubnetApi;
import org.jclouds.openstack.nova.v2_0.NovaApi;
import org.jclouds.openstack.nova.v2_0.compute.options.NovaTemplateOptions;
import org.jclouds.openstack.nova.v2_0.domain.KeyPair;
import org.jclouds.openstack.nova.v2_0.extensions.KeyPairApi;
import org.jclouds.scriptbuilder.ScriptBuilder;
import org.jclouds.scriptbuilder.domain.LiteralStatement;
import org.jclouds.scriptbuilder.domain.Statement;
import org.jclouds.scriptbuilder.statements.login.UserAdd;
import org.jclouds.scriptbuilder.statements.ssh.AuthorizeRSAPublicKeys;
import org.jclouds.scriptbuilder.statements.ssh.InstallRSAPrivateKey;
import org.jclouds.ssh.jsch.config.JschSshClientModule;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Module;

import eu.specs.project.enforcement.broker.ScriptExecutionResult.ScriptExecutionOutcome;
import eu.specs.project.enforcement.broker.entities.InstanceDescriptor;
import eu.specs.project.enforcement.broker.entities.NodesInfo;
import eu.specs.project.enforcement.broker.entities.credentials.NodeCredential;
import eu.specs.project.enforcement.broker.entities.credentials.ProviderCredential;

public class CloudServiceComputeOpenStack {
	private ComputeServiceContext context;
	private ComputeService compute;
	private String defaultUser;
	private Template template;
	
	private InstanceDescriptor globalInstanceDescriptor;
	private String globalNetworkId;
	private String globalGroupName;
	private int[] globalInboudports;

	private final NovaApi novaApi;
	private NeutronApi neutronApi;
	
	/**
	 * Represents a Cloud Service Provider, it give access to creation and management of nodes.
	 * It uses jcloud to provide service over different cloud platforms
	 * @param provider identifier of the provider 
	 * @param defaultUser 
	 * @param providerCredential
	 */
	public CloudServiceComputeOpenStack(String provider, String defaultUser, ProviderCredential providerCredential){
		
		Properties properties = new Properties();
	    long scriptTimeout = TimeUnit.MILLISECONDS.convert(20, TimeUnit.MINUTES);
	    properties.setProperty(TIMEOUT_SCRIPT_COMPLETE, scriptTimeout + "");
	    this.defaultUser = defaultUser;
	    
	    context = ContextBuilder.newBuilder("openstack-nova")
	    		.endpoint(provider)
                .credentials(providerCredential.getUsername(), providerCredential.getPassword())
                .overrides(properties)
                .modules(ImmutableSet.<Module> of(
                		 new JschSshClientModule()))
                .buildView(ComputeServiceContext.class); 
	    compute = context.getComputeService();
		
		Properties overrides = new Properties();
		overrides.setProperty(KeystoneProperties.CREDENTIAL_TYPE, CredentialTypes.PASSWORD_CREDENTIALS);
		overrides.setProperty(Constants.PROPERTY_RELAX_HOSTNAME, "true");
		overrides.setProperty(Constants.PROPERTY_TRUST_ALL_CERTS, "true");
		
//		novaApi = ContextBuilder.newBuilder("openstack-nova")
//                .endpoint(provider)
//                .credentials(providerCredential.getUsername(), providerCredential.getPassword())
//                .overrides(overrides)
//                .modules(ImmutableSet.<Module> of(new JschSshClientModule()))
//                .buildApi(NovaApi.class);
		novaApi = context.unwrapApi(NovaApi.class);
		
        //instance for making request to the neutron component "network component"
        neutronApi = ContextBuilder.newBuilder("openstack-neutron")
                .endpoint(provider)
                .credentials(providerCredential.getUsername(), providerCredential.getPassword())
                //.modules(modules)
                .overrides(overrides)
                .buildApi(NeutronApi.class);    	    
	    
	}
	
	
	private Template obtainTemplate(InstanceDescriptor descriptor, String publicKey, String networkId, String groupName, int... inboudports){
		  TemplateBuilder templateBuilder = compute.templateBuilder();

		  NovaTemplateOptions options = NovaTemplateOptions.Builder.blockUntilRunning(true);
		  options.keyPairName(publicKey);
		  options.securityGroups(groupName);
		  options.autoAssignFloatingIp(true);
		  options.inboundPorts(inboudports);
		  options.networks(networkId);
		  options.blockOnPort(22, 300);		//wait until the SSH port is ready for connect
		  options.overrideLoginCredentials(LoginCredentials.builder().authenticateSudo(true).password("password").noPrivateKey().build());

		  		  
		  template = templateBuilder
		    .imageId(descriptor.getImage())
		    .hardwareId(descriptor.getHardwareId())
		    .locationId(descriptor.getLocation())
		    .options(options)
		    .build();
		  return template;
	}
	
	
	
	/**
	 * creates multiple nodes in a Cloud Service Provider
	 * @param groupName string that represent the group to which the machines are assigned 
	 * @param numberOfInstances number of instances to create 
	 * @param descriptor object that contain information about the appliance, hw and zone
	 * @param nodeCredential object containing root password private and public key
	 * @param inboudports inbound ports to open on the created machines 
	 * @return information about all the nodes created and the credentials
	 * @throws NoSuchElementException
	 * @throws Exception
	 */
	public NodesInfo createNodesInGroup(String groupName, int numberOfInstances, InstanceDescriptor descriptor, NodeCredential nodeCredential, String publicKey, String networkId, int... inboudports)  throws NoSuchElementException, Exception {	
		globalInstanceDescriptor = descriptor;
		globalNetworkId = networkId;
		globalGroupName = groupName;
		globalInboudports = inboudports;
		
		// TODO: remove the delete keypairs and deleteAllGroupsExceptDefault they are used only for testing
		deleteAllKeypairs();
		//TODO: try to insert the creation of the keypair into the creation of the machine "obtainTemplate"
		KeyPair keyPair = this.createKeyPair(groupName+"Key", publicKey);
		
		Set<? extends NodeMetadata> nodes = null;
		try {
			template = obtainTemplate(descriptor, keyPair.getName(), networkId, groupName, inboudports);
			nodes = compute.createNodesInGroup(groupName, numberOfInstances, template);
			Thread.sleep(5000);
		}catch(NoSuchElementException e){
			throw new NoSuchElementException(e.getMessage());
		}
		catch (Exception e) {
			destroyClusterWithName(groupName);
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		NodesInfo info = new NodesInfo();
		for(NodeMetadata n : nodes){
			info.addNode(new ClusterNode(n.getId(), n.getPublicAddresses().iterator().next(), n.getPrivateAddresses().iterator().next()));
		}
	
		InstallRSAPrivateKey instPriv = new InstallRSAPrivateKey(nodeCredential.getPrivatekey());
		
		ScriptBuilder sb = new ScriptBuilder();
		
		sb.addStatement(instPriv);
		ExecResponse er = null;
				
		for(NodeMetadata n : nodes){
			try {
				er = compute.runScriptOnNode(n.getId(), sb, overrideLoginCredentials(getLoginForCommandExecution(defaultUser, nodeCredential.getPrivatekey())).runAsRoot(false).wrapInInitScript(false));
				
			}catch(Exception exception){
				exception.printStackTrace();
			}
			//verify if the statement execute with success otherwise destroy the cluster
			if(er.getExitStatus()!=0){
				destroyClusterWithName(groupName);
				throw new Exception("Error in configuration of custom RSA keys");
			}
		}
		
		//assign a new password to the default user, the password is the same for all the nodes
		LiteralStatement enablePasswordSsh = new LiteralStatement(new String("exec 3<> /etc/ssh/sshd_config && awk '{ gsub(\"PasswordAuthentication\",\"#PasswordAuthentication\"); print }' /etc/ssh/sshd_config >&3 hash service 2>&- && (service ssh reload 2>&- || /etc/init.d/ssh* reload) || rcsshd restart"));
		LiteralStatement pass = new LiteralStatement(String.format("echo -e \"%s\n%s\n\" | passwd root", nodeCredential.getRootpassword(),nodeCredential.getRootpassword()));
		sb = new ScriptBuilder();
		
		sb.addStatement(pass);
		sb.addStatement(enablePasswordSsh);
		

		for(NodeMetadata n : nodes){
			er = compute.runScriptOnNode(n.getId(), sb, overrideLoginCredentials(getLoginForCommandExecution(defaultUser, nodeCredential.getPrivatekey())).runAsRoot(true).wrapInInitScript(false));
			if(er.getExitStatus()!=0){
				destroyClusterWithName(groupName);
				throw new Exception("Error in installing root password");
			}
		}
		
		info.setPublicKey(nodeCredential.getPublickey());
		info.setPrivateKey(nodeCredential.getPrivatekey());
		info.setRootPassword(nodeCredential.getRootpassword());
		return info;
	}
	
	
	public ClusterNode addNode(String groupName, String pubKey, String privKey, String rootPassword) throws Exception{
		NodeMetadata n = null;
		try{
			//generate again the template with the new public key
			template = obtainTemplate(globalInstanceDescriptor, pubKey, globalNetworkId, groupName, globalInboudports);
			//use the template for generate a new node in the group
			n = getOnlyElement(compute.createNodesInGroup(groupName, 1, template));
		} catch (RunNodesException e) {
			throw new Exception(e.getMessage());
		} finally{
			destroyClusterWithName(groupName);
		}
		//HashSet<String> pubKeys = new HashSet<String>();
		//pubKeys.add(pubKey);
		//AuthorizeRSAPublicKeys authKeys = new AuthorizeRSAPublicKeys(pubKeys);
		InstallRSAPrivateKey instPriv = new InstallRSAPrivateKey(privKey);
		
		ScriptBuilder sb = new ScriptBuilder();

		//sb.addStatement(authKeys);
		sb.addStatement(instPriv);
		ExecResponse er = compute.runScriptOnNode(n.getId(), sb, 
			overrideLoginCredentials(getLoginForCommandExecution(defaultUser, n.getCredentials().credential)).runAsRoot(false).wrapInInitScript(false));
		if(er.getExitStatus()!=0){
			destroyClusterWithName(groupName);
			throw new Exception("Error in configuration of custom RSA keys");
		}
		
		LiteralStatement pass = new LiteralStatement(String.format("echo \"%s\" | passwd --stdin root", rootPassword));
		sb = new ScriptBuilder();
		sb.addStatement(pass);
		er = compute.runScriptOnNode(n.getId(), sb, 
			overrideLoginCredentials(getLoginForCommandExecution(defaultUser, n.getCredentials().credential)).runAsRoot(true).wrapInInitScript(false));
		if(er.getExitStatus()!=0){
			destroyClusterWithName(groupName);
			throw new Exception("Error in installing root password");
		}
		
		return new ClusterNode(n.getId(), n.getPublicAddresses().iterator().next(), n.getPrivateAddresses().iterator().next());
	}
	
	
	
	/**
	 * 
	 * @param userName
	 * @param password
	 * @param newPublicKey
	 * @param newPrivateKey
	 * @param nodes
	 * @param privateKeyDefaultUser
	 * @return
	 */
	public ScriptExecutionResult addNewUser(String userName, String password, String newPublicKey, String newPrivateKey, List<ClusterNode> nodes, String privateKeyDefaultUser){
		HashMap<String, String> info = new HashMap<String, String>();
		ScriptExecutionResult scriptResult = new ScriptExecutionResult();
		UserAdd.Builder userBuilder = UserAdd.builder();
        userBuilder.login(userName);
        userBuilder.authorizeRSAPublicKey(newPublicKey);
        userBuilder.installRSAPrivateKey(newPrivateKey);
        userBuilder.defaultHome("/home");
        if(password!=null){
        	userBuilder.password(password);
        }
        Statement userBuilderStatement = userBuilder.build();
        ExecResponse er = null;
       
        for(ClusterNode n : nodes){
        	er = compute.runScriptOnNode(n.getId(), userBuilderStatement,
    			overrideLoginCredentials(getLoginForCommandExecution(defaultUser, privateKeyDefaultUser)).runAsRoot(true).wrapInInitScript(false)); 
        	if(er.getExitStatus()!=0){
        		scriptResult.setOutcome(ScriptExecutionOutcome.GENERIC_ERROR);
				scriptResult.addAdditionalInfo("Error_Type", "New user configuration error");
				scriptResult.addNodeWithError(n);
			}
        }
        info.put("privateKeyNewUser", newPrivateKey);
        info.put("publicKeyNewUser", newPublicKey);
        info.put("username", userName);
        info.put("password", password);
        scriptResult.setAdditionalInfo(info);
        return scriptResult;
	}
	
	

		
	public ScriptExecutionResult executeScriptOnNode(String user, ClusterNode node, String[] instructions, String privateKey, boolean sudo){
		ScriptExecutionResult scriptResult = new ScriptExecutionResult();
		ScriptBuilder sb = new ScriptBuilder();
		for(String s : instructions){
			sb.addStatement(new LiteralStatement(s));
		}
		ExecResponse er = compute.runScriptOnNode(node.getId(), sb,
				overrideLoginCredentials(getLoginForCommandExecution(user, privateKey)).runAsRoot(sudo).wrapInInitScript(false));
		if(er.getExitStatus()!=0){
    		scriptResult.setOutcome(ScriptExecutionOutcome.GENERIC_ERROR);
			scriptResult.addAdditionalInfo("Error_Type", "New user configuration error");
			scriptResult.addNodeWithError(node);
		}
		scriptResult.addOutput(node.getId(), er.getOutput());
		return scriptResult;
	}
	
	
	public ScriptExecutionResult executeScriptOnNode(String user, ClusterNode node, Statement s, String privateKey, boolean sudo){
		ScriptExecutionResult scriptResult = new ScriptExecutionResult();
		ScriptBuilder sb = new ScriptBuilder();
		sb.addStatement(s);
		ExecResponse er = compute.runScriptOnNode(node.getId(), sb,
				overrideLoginCredentials(getLoginForCommandExecution(user, privateKey)).runAsRoot(sudo).wrapInInitScript(false));
		if(er.getExitStatus()!=0){
    		scriptResult.setOutcome(ScriptExecutionOutcome.GENERIC_ERROR);
			scriptResult.addAdditionalInfo("Error_Type", "New user configuration error");
			scriptResult.addNodeWithError(node);
		}
		scriptResult.addOutput(node.getId(), er.getOutput());
		return scriptResult;
	}
	
	public Map<String, ScriptExecutionResult> executeScriptOnNodes(String user, List<ClusterNode> nodes, String[] instructions, String privateKey, boolean sudo){
		Map<String, ScriptExecutionResult> map = new HashMap<String, ScriptExecutionResult>();
		ScriptExecutionResult temp;
		for(ClusterNode n : nodes){
			temp = executeScriptOnNode(user, n, instructions, privateKey, sudo);
			map.put(n.getId(), temp);
		}
		return map;
	}
	
	public Map<String, ScriptExecutionResult> executeScriptOnNodes(String user, List<ClusterNode> nodes,Statement s, String privateKey, boolean sudo){
		Map<String, ScriptExecutionResult> map = new HashMap<String, ScriptExecutionResult>();
		ScriptExecutionResult temp;
		for(ClusterNode n : nodes){
			temp = executeScriptOnNode(user, n, s, privateKey, sudo);
			map.put(n.getId(), temp);
		}
		return map;
	}

	public List<Hardware> getHardwareTypes(){
		
		Set<? extends Hardware> flavors = compute.listHardwareProfiles();
		List<Hardware> listHardware = new ArrayList<Hardware>(flavors);
		System.out.println("Flavor List");
		for (Hardware flavor : flavors){
			System.out.println("Flavor id : "+ flavor.getId());
			System.out.println("Flavor name : "+ flavor.getName());
			System.out.println("Flavor prov : "+ flavor.getProviderId());
		}
		return listHardware;
	
	}

	public List<Image> getAppliances(){
		
		Set<? extends Image> images = compute.listImages();
		List<Image> listImage = new ArrayList<Image>(images);
		System.out.println("Image List");
		for (Image image : images){
			System.out.println("Image id : "+ image.getId());
			System.out.println("Image name : "+ image.getName());
			System.out.println("Image desc : "+ image.getDescription());
		}

		return listImage;
	}

	public List<Location> getAvailabilityZones(){
		
		Set<? extends Location> locations = compute.listAssignableLocations();
		List<Location> listLocation = new ArrayList<Location>(locations);
		System.out.println("Location List");
		for (Location location : locations){
			System.out.println("Location id : "+ location.getId());
			System.out.println("Location desc : "+ location.getDescription());
		}
		return listLocation;
	}

	
//	private SshClient getSshClientForNode(String nodeId, String user, String credential){
//		NodeMetadata node = compute.getNodeMetadata(nodeId);
//		return context.utils().sshForNode()
//				.apply(NodeMetadataBuilder.fromNodeMetadata(node)
//				.credentials(getLoginForCommandExecution(user, credential))
//				.build());
//	}
	
	//provare con rackspace start stop reboot
	public void suspendNodesInGroup(List<ClusterNode> hosts){
		for (int i = 0; i<hosts.size(); i++){
			compute.suspendNode(hosts.get(i).getId());
		}
	}
	
	public void resumeNodesInGroup(List<ClusterNode> hosts){
		for (int i = 0; i<hosts.size(); i++){
			compute.resumeNode(hosts.get(i).getId());
		}
	}

	public void rebootNodesInGroup(List<ClusterNode> hosts){
		for (int i = 0; i<hosts.size(); i++){
			compute.rebootNode(hosts.get(i).getId());
		}
	}
	
	//provare con versione 1.7
	public void destroyCluster(List<ClusterNode> hosts){ //distrugge anche keypair associati e il security group se non ci sono altre risorse pendenti associate
		for (int i = 0; i<hosts.size(); i++){
			compute.destroyNode(hosts.get(i).getId());
		}
	}
	
	public void destroyClusterWithName(String groupName){
		try{
			compute.destroyNodesMatching(and(inGroup(groupName), not(TERMINATED)));
		} catch(Exception e){
			e.getMessage();
		}
	}
	
	public void destroySingleNode(ClusterNode node){
		compute.destroyNode(node.getId());
	}
		
	private LoginCredentials getLoginForCommandExecution(String user, String credential) {
	
	    return LoginCredentials.builder().noPassword().user(user).credential(credential).build();   
	}


	
//	private ExecResponse executeSshCommand(SshClient ssh, String cmd){
//		return ssh.exec(cmd);
//	}	
		
	
public class ExecuteStatementsOnNode  extends Thread{
		
		public ExecuteStatementsOnNode(String user, ClusterNode node,
				Statement statement, String privateKey, boolean sudo,
				CloudServiceComputeOpenStack compute) {
			super();
			this.user = user;
			this.node = node;
			this.statement = statement;
			this.privateKey = privateKey;
			this.sudo = sudo;
			this.compute = compute;
		}

		private String user;
		private ClusterNode node;
		private Statement statement;
		private String privateKey;
		boolean sudo;
		private CloudServiceComputeOpenStack compute;
		
		public void run(){ 
		
			compute.executeScriptOnNode(user, node,statement,privateKey,sudo);
		
		}
		}
	
	public class ExecuteIstructionsOnNode  extends Thread{
		
		private String user;
		private ClusterNode node;
		private String[] istructions;
		private String privateKey;
		boolean sudo;
		private CloudServiceComputeOpenStack compute;
		
		public ExecuteIstructionsOnNode(String user, ClusterNode node,
				String[] istructions, String privateKey, boolean sudo,
				CloudServiceComputeOpenStack compute) {
			super();
			this.user = user;
			this.node = node;
			this.istructions = istructions;
			this.privateKey = privateKey;
			this.sudo = sudo;
			this.compute = compute;
		}
		
		public void run(){ 
		
			compute.executeScriptOnNode(user, node,istructions,privateKey,sudo);
		
		}
	}
	
	private void deleteAllKeypairs()	{
		KeyPairApi keyPairApi = novaApi.getKeyPairApi("RegionOne").get();
		Iterator<KeyPair> keyPairIterator = keyPairApi.list().iterator();
		while (keyPairIterator.hasNext()){
			
			String keyPairName = keyPairIterator.next().getName();
			keyPairApi.delete(keyPairName);
			System.out.println("Deleted key : " + keyPairName);
			
	    }

	}
	
	private void deleteAllGroupsExceptDefault()	{
		SecurityGroupApi securityGroupApi = neutronApi.getSecurityGroupApi("RegionOne").get();
		ImmutableList<SecurityGroup> securityGroups = securityGroupApi.listSecurityGroups().concat().toList();
		for (SecurityGroup securityGroupElement : securityGroups){
			if (!securityGroupElement.getName().equalsIgnoreCase("default")){
				System.out.println("Delete Security Group : " + securityGroupElement.getName());
				securityGroupApi.deleteSecurityGroup(securityGroupElement.getId());
			}
		}
	}
	
	private void deleteKeypair(String keyPairName)	{
		try{
			KeyPairApi keyPairApi = novaApi.getKeyPairApi("RegionOne").get();
			keyPairApi.delete(keyPairName);
			System.out.println("Deleted key : " + keyPairName);
		}catch(Exception e){
			System.out.println("Cannot delete key : " + keyPairName);
			e.printStackTrace();
		}

		

	}
	
	public void deleteSecurityGroup(String securityGroupName)	{
		try{
			SecurityGroupApi securityGroupApi = neutronApi.getSecurityGroupApi("RegionOne").get();
			ImmutableList<SecurityGroup> securityGroups = securityGroupApi.listSecurityGroups().concat().toList();
			for (SecurityGroup securityGroupElement : securityGroups){
				if (securityGroupElement.getName().equalsIgnoreCase(securityGroupName)){
					System.out.println("Delete Security Group : " + securityGroupElement.getName());
					securityGroupApi.deleteSecurityGroup(securityGroupElement.getId());
				}
			}
		}catch(Exception e){
			System.out.println("Cannot delete security group : " + securityGroupName);
			e.printStackTrace();
		}
		
	}
	
	public KeyPair createKeyPair(String name, String publickey) {
		KeyPairApi keyPairApi = novaApi.getKeyPairApi("RegionOne").get();
		KeyPair keyPair = keyPairApi.createWithPublicKey(name, publickey);
		
		return keyPair;
	}
	
	public boolean createSecurityGroup(String groupName, String description){
		try{
			SecurityGroupApi securityGroupApi = neutronApi.getSecurityGroupApi("RegionOne").get();
			CreateSecurityGroup createSecurityGroup = CreateSecurityGroup.createBuilder()
					.description(description)
					.name(groupName)
					.build();
			SecurityGroup securityGroup = securityGroupApi.create(createSecurityGroup);
			//Ingress TCP rule
			CreateRule createRule = CreateRule.createBuilder(RuleDirection.INGRESS, securityGroup.getId())
					.ethertype(RuleEthertype.IPV4).portRangeMin(0).portRangeMax(65535).protocol(RuleProtocol.TCP)
					.build();
			securityGroupApi.create(createRule);
			//Egress TCP rule
			createRule = CreateRule.createBuilder(RuleDirection.EGRESS, securityGroup.getId())
					.ethertype(RuleEthertype.IPV4).portRangeMin(0).portRangeMax(65535).protocol(RuleProtocol.TCP)
					.build();
			securityGroupApi.create(createRule);
			//Ingress ICMP rule
			createRule = CreateRule.createBuilder(RuleDirection.INGRESS, securityGroup.getId())
					.ethertype(RuleEthertype.IPV4).protocol(RuleProtocol.ICMP)
					.build();
			securityGroupApi.create(createRule);
			//Egress ICMP rule
			createRule = CreateRule.createBuilder(RuleDirection.EGRESS, securityGroup.getId())
					.ethertype(RuleEthertype.IPV4).protocol(RuleProtocol.ICMP)
					.build();
			securityGroupApi.create(createRule);
			

		}catch(Exception e){
			System.out.println("Cannot create security group : " + groupName);
			e.printStackTrace();
			return false;
		}
		return true;
		
	}
	
	private boolean deleteNetwork(String networkName){
		try{
			NetworkApi networkApi = neutronApi.getNetworkApi("RegionOne");
			List<Network> networkList = networkApi.list().concat().toList();
			for (Network network : networkList){
				if (network.getName().equalsIgnoreCase(networkName)){
					networkApi.delete(network.getId());
				}
			}
			
			return true;
		}catch (Exception e){
			System.out.println("Cannot remove the network "+networkName);
			e.printStackTrace();
			return false;
		}

	}
	
	private boolean deleteRouter(String routerName, String subnetName){
		try{
			String subnetId = null;
			NetworkApi networkApi = neutronApi.getNetworkApi("RegionOne");
			List<Network> networkList = networkApi.list().concat().toList();
			for (Network network : networkList){
				if (network.getName().equalsIgnoreCase(subnetName)){
					subnetId = network.getId();
				}

			}
			
			PortApi portApi = neutronApi.getPortApi("RegionOne");
			List<Port> portList = portApi.list().concat().toList();

			RouterApi routerApi = neutronApi.getRouterApi("RegionOne").get();
			List<Router> routerList = routerApi.list().concat().toList();
			for (Router router : routerList){
				if (router.getName().equalsIgnoreCase(routerName)){
					
					
					
					routerApi.removeInterfaceForSubnet(router.getId(), subnetId);
					
					String routerId = router.getId();
					
					for (Port port : portList){
						if (port.getDeviceId().equalsIgnoreCase(routerId)){
							
							routerApi.removeInterfaceForPort(routerId, port.getId());
							
							portApi.delete(port.getId());
							
						}
					}
					
					
					
					routerApi.delete(router.getId());
				}
			}
			
			return true;
		}catch (Exception e){
			System.out.println("Cannot remove the router "+routerName);
			e.printStackTrace();
			return false;
		}

	}
	
	public void connectNetworkToExtern(String routerName, String networkId){
		
		String externalAddressId = getExternalAddressId();
		
		RouterApi routerApi = neutronApi.getRouterApi("RegionOne").get();
		ExternalGatewayInfo externalGatewayInfo = ExternalGatewayInfo.builder().networkId(externalAddressId).build();
		CreateRouter createRouter = CreateRouter.createBuilder().name(routerName).adminStateUp(true)
				.externalGatewayInfo(externalGatewayInfo).build();
		
		Router router = routerApi.create(createRouter);
		
		RouterInterface routerInterface = routerApi.addInterfaceForSubnet(router.getId(), networkId);
		
	}
	
	public Network createNetwork(String networkName){
		NetworkApi networkApi = neutronApi.getNetworkApi("RegionOne");
		CreateNetwork createNetwork = Network.createBuilder(networkName).adminStateUp(true).build();
		
		return networkApi.create(createNetwork);
	}
	
	public Subnet createSubnet(String networkId, String cidr){
		SubnetApi subnetApi = neutronApi.getSubnetApi("RegionOne");
		
		CreateSubnet createSubnet = CreateSubnet.createBuilder(networkId, cidr).ipVersion(4).enableDhcp(true).build();
		
		return subnetApi.create(createSubnet);
	}
	
	private String getExternalAddressId(){
		try{
			NetworkApi networkApi = neutronApi.getNetworkApi("RegionOne");
			List<Network> networkList = networkApi.list().concat().toList();
			for (Network network : networkList){
				if (network.getExternal()){
					return network.getId();
				}
			}
		}catch(Exception e){
			System.out.println("Cannot find the id of the external network");
			e.printStackTrace();
		}
		return null;
		
	}
	
	private boolean deleteGroupInstances(String groupName){
		try{
			compute.destroyNodesMatching(NodePredicates.inGroup(groupName));
		}catch(Exception e){
			System.out.println("Cannot delete all the instances in the group : "+groupName);
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private boolean deallocateAllFloatingIP(){
		try{
			FloatingIPApi floatingApi = neutronApi.getFloatingIPApi("RegionOne").get();
			List<FloatingIP> floatingList = floatingApi.list().concat().toList();
			for (FloatingIP floatingIP : floatingList){
				floatingApi.delete(floatingIP.getId());
			}
		}catch(Exception e){
			System.out.println("Cannot delete the floating IP");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean clearGroupNodesAndNetwork(String routerName, String networkName, String groupName){
		deleteGroupInstances(groupName);
		deleteRouter(routerName, networkName);
		
		deleteNetwork(networkName);
		
		deleteSecurityGroup(groupName);
		
		deallocateAllFloatingIP();
		
		
		return true;
	}
	
	

	
	
}
