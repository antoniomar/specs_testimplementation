/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specs.project.enforcement.broker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScriptExecutionResult {

	private ScriptExecutionOutcome outcome;
	private Map<String, String> additionalInfo;
	private Map<String, String> outputMap;
	private List<ClusterNode> nodesWithError;
	
	public ScriptExecutionResult(){
		additionalInfo = new HashMap<String, String>();
		nodesWithError = new ArrayList<ClusterNode>();
		outputMap = new HashMap<String, String>();
		outcome = ScriptExecutionOutcome.OK;
	}
	
	public ScriptExecutionResult(ScriptExecutionOutcome outcome,
			Map<String, String> additionalInfo, List<ClusterNode> nodesWithError, Map<String, String> outputMap) {
		super();
		this.outcome = outcome;
		this.additionalInfo = additionalInfo;
		this.outputMap = outputMap;
		this.nodesWithError = nodesWithError;
	}


	public ScriptExecutionOutcome getOutcome() {
		return outcome;
	}

	public void setOutcome(ScriptExecutionOutcome outcome) {
		this.outcome = outcome;
	}

	public Map<String, String> getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(Map<String, String> additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	
	public void addAdditionalInfo(String key, String value){
		additionalInfo.put(key, value);
	}

	public List<ClusterNode> getNodesWithError() {
		return nodesWithError;
	}

	public void setNodesWithError(List<ClusterNode> nodesWithError) {
		this.nodesWithError = nodesWithError;
	}
	
	public void addNodeWithError(ClusterNode nodeId){
		nodesWithError.add(nodeId);
	}
	
	public Map<String, String> getOutputMap() {
		return outputMap;
	}

	public void setOutputMap(Map<String, String> outputMap) {
		this.outputMap = outputMap;
	}

	public void addOutput(String key, String output){
		outputMap.put(key, output);
	}

	public enum ScriptExecutionOutcome{
		OK, MASTER_ERROR, SLAVE_ERROR, GENERIC_ERROR;
	}
}
