/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specs.project.enforcement.broker;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.jclouds.ContextBuilder;
import org.jclouds.chef.ChefApi;
import org.jclouds.chef.ChefContext;
import org.jclouds.chef.config.ChefProperties;
import org.jclouds.chef.domain.BootstrapConfig;
import org.jclouds.chef.domain.DatabagItem;
import org.jclouds.chef.domain.Node;
import org.jclouds.domain.JsonBall;
import org.jclouds.scriptbuilder.domain.Statement;

import eu.specs.project.enforcement.broker.entities.NodesInfo;
import eu.specs.project.enforcement.broker.entities.credentials.NodeCredential;


public class ChefService {

	public ChefApi api;
	private ChefContext context;

	/**
	 * Create a class for communicating with the Chef Server
	 * @param organization organization associated with the chef user
	 * @param organizationPK private key of the organization
	 * @param chefServerEndpoint address of the chef server
	 * @param username login name to access the chef server
	 * @param passwordPK private password to access the chef server
	 */
	public ChefService (String organization, String organizationPK, String chefServerEndpoint, String username, String passwordPK) {

		Properties chefConfig = new Properties();
		chefConfig.put(ChefProperties.CHEF_VALIDATOR_NAME, organization+"-validator");
		chefConfig.put(ChefProperties.CHEF_VALIDATOR_CREDENTIAL, organizationPK );
		this.context = ContextBuilder.newBuilder("chef").
				endpoint(chefServerEndpoint + organization).
				credentials( username, passwordPK  ).
				overrides(chefConfig).buildView(ChefContext.class);

		this.api = context.unwrapApi(ChefApi.class);
	}


	public ChefNodeInfo bootstrapChef(String group, NodesInfo nodes,CloudService cloudservice,String attribute) {

		JsonBall attrs=new JsonBall(attribute);

	//	BootstrapConfig bootConfig = BootstrapConfig.builder().attributes(attrs).environment(group).build();
		BootstrapConfig bootConfig = BootstrapConfig.builder().attributes(attrs).build();
		context.getChefService().updateBootstrapConfigForGroup(group, bootConfig);
		Statement bootstrap =  context.getChefService().createBootstrapScriptForGroup(group);
		
	//	Environment env = Environment.builder().name(group).build();
	//	api.createEnvironment(env);
		
		Thread[] threads = new Thread[nodes.getNodes().size()];
		for (int i = 0; i < threads.length; i++) {
		threads[i] = cloudservice.new ExecuteStatementsOnNode("root",nodes.getNodes().get(i),bootstrap,nodes.getPrivateKey(),false,cloudservice);
		threads[i].start();
		}
		
		for (int i = 0; i < threads.length; i++) {
		try {
		threads[i].join();
		} catch (InterruptedException ignore) {}
		}
		
//		cloudservice.executeScriptOnNodes("root",nodes.getNodes(),bootstrap,nodes.getPrivateKey(),false);
	
		return new ChefNodeInfo();

	}

	 public void  executeRecipesOnNode(ClusterNode node, List<String> runList,String group,CloudService compute,NodeCredential nodecred) {
			 
		    Node oldnode = api.getNode(group+"-"+node.getPrivateIP());		 
			Node updated = Node.builder()
					.name(group+"-"+node.getPrivateIP())
					.automaticAttributes(oldnode.getAutomaticAttributes())
					.defaultAttributes(oldnode.getDefaultAttributes())
					.environment(oldnode.getEnvironment())
					.normalAttributes(oldnode.getNormalAttributes())
					.overrideAttributes(oldnode.getOverrideAttributes())
					.runList(runList)
					.build();
			
			api.updateNode(updated);
			
			String[] active_client= {"chef-client > /var/log/chefclient.log"};	
			compute.executeScriptOnNode("root", node,active_client,nodecred.getPrivatekey(),false);
		
		}
	 

	public void  executeRecipesOnNodes(List<ClusterNode> nodes , List<String> runList,String group,CloudService compute,NodeCredential nodecred) {
		
		for(ClusterNode node : nodes)
		
		executeRecipesOnNode(node,runList,group,compute,nodecred);
		
		
		
		
	
	}

  

	public class ChefNodeInfo {
		
		

	}
	
	
	
	
public class ExecuteRecipesOnNode  extends Thread{
		
		
		private ClusterNode node;
		private List<String> runList;
		private String group;
		private String privateKey;
		private CloudService compute;
		
				
		public ExecuteRecipesOnNode(ClusterNode node,
				List<String> runList, String group, String privateKey,
				CloudService compute) {
			super();
			
			this.node = node;
			this.runList = runList;
			this.group = group;
			this.privateKey = privateKey;
			this.compute = compute;
		}


		public void run(){ 
		
			 Node oldnode = api.getNode(group+"-"+node.getPrivateIP());		 
				Node updated = Node.builder()
						.name(group+"-"+node.getPrivateIP())
						.automaticAttributes(oldnode.getAutomaticAttributes())
						.defaultAttributes(oldnode.getDefaultAttributes())
						.environment(oldnode.getEnvironment())
						.normalAttributes(oldnode.getNormalAttributes())
						.overrideAttributes(oldnode.getOverrideAttributes())
						.runList(runList)
						.build();
				
				api.updateNode(updated);
				
				String[] active_client= {"chef-client > /var/log/chefclient.log"};	
				compute.executeScriptOnNode("root", node,active_client,privateKey,false);
				java.util.Date date= new java.util.Date();	
				System.out.println(new Timestamp(date.getTime())+" | completed recipes: "+Arrays.toString(runList.toArray())+" on node "+group+"-"+node.getPrivateIP()+" publicIP:"+node.getPublicIP());				
				
		
		}
		}
	
	
	public void uploadDatabagItem(String databagName,String databagItemId,String databagItemValue){
		
		DatabagItem item =new DatabagItem(databagItemId,databagItemValue);	
		api.createDatabagItem(databagName, item);
		
		
	}
	
	public DatabagItem getDatabagItem(String databagName, String databagItemId){
		return api.getDatabagItem(databagName, databagItemId);
	}
	
	
}
