/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specs.project.enforcement.broker.plan;

import java.util.List;
/**
 * Contain a reference to all the nodes instantiate on the Cloud Service Provider as a list of Nodes 
 * @see Nodes
 */
public class VMs{
   	private List nodes;
   	private String nodes_access_credentials_reference;

 	public List getNodes(){
		return this.nodes;
	}
	public void setNodes(List nodes){
		this.nodes = nodes;
	}
 	public String getNodes_access_credentials_reference(){
		return this.nodes_access_credentials_reference;
	}
	public void setNodes_access_credentials_reference(String nodes_access_credentials_reference){
		this.nodes_access_credentials_reference = nodes_access_credentials_reference;
	}
}
