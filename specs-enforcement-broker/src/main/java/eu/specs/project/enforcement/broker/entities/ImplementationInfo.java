package eu.specs.project.enforcement.broker.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public  class ImplementationInfo {
	
//	public  enum  Status {Working, Ready};
	public  enum  Status {Starting_Implementation, VMs_Acquired,VMs_Prepared,Chef_Node_Bootstrapped,Recipes_completed,Ready};
	public Status status;
	public String SLAID;
	public int instances;
	public List<String> private_IP_Address;
	public List<String> public_IP_Address;
	public List<String> chefNodesName;
	public List<List<String>> recipes;
	public Map <String,List<MetricInfo>> metrics;
	
	
	public Map <String,List<MetricInfo>> getMetrics() {
		return metrics;
	}

	public void setMetrics(Map <String,List<MetricInfo>> metrics) {
		this.metrics = metrics;
	}

	public ImplementationInfo () {}
	
	public ImplementationInfo(String sLAID) {
		super();
		SLAID = sLAID;
		
		this.private_IP_Address=new ArrayList<String>();
		this.public_IP_Address=new ArrayList<String>();
		this.chefNodesName=new ArrayList<String>();
		this.recipes=new ArrayList<List<String>>();
		this.metrics = new HashMap<String, List<MetricInfo>> ();
		
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getSLAID() {
		return SLAID;
	}

	public void setSLAID(String sLAID) {
		SLAID = sLAID;
	}

	public int getInstances() {
		return instances;
	}

	public void setInstances(int instances) {
		this.instances = instances;
	}

	public List<String> getPrivate_IP_Address() {
		return private_IP_Address;
	}

	public void setPrivate_IP_Address(List<String> private_IP_Address) {
		this.private_IP_Address = private_IP_Address;
	}

	public List<String> getPublic_IP_Address() {
		return public_IP_Address;
	}

	public void setPublic_IP_Address(List<String> public_IP_Address) {
		this.public_IP_Address = public_IP_Address;
	}

	public List<String> getChefNodesName() {
		return chefNodesName;
	}

	public void setChefNodesName(List<String> chefNodesName) {
		this.chefNodesName = chefNodesName;
	}

	public List<List<String>> getRecipes() {
		return recipes;
	}

	public void setRecipes(List<List<String>> recipes) {
		this.recipes = recipes;
	} 
}