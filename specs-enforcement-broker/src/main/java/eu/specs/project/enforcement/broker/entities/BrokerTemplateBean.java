/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specs.project.enforcement.broker.entities;

import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.gson.Gson;

public class BrokerTemplateBean {
	
	public String id;
	public String description;
	public String provider;
	public String zone;
	public String hw;
	public String appliance;
	
	
	public static BrokerTemplateBean create(String json){
		return new Gson().fromJson(json, BrokerTemplateBean.class);
	}

	
	public static void main(String[] args) throws Exception {
		
		 byte[] encoded = Files.readAllBytes(Paths.get("C:\\Users\\mauro\\Desktop\\mosaic workspace\\SpecsApplication_SWS_Demo\\src\\resources\\broker_templates\\aws_open_suse.json"));		
	
		 System.out.println(new String(encoded));
		
		System.out.println(new Gson().toJson(create( new String(encoded))));
		
	}
}
