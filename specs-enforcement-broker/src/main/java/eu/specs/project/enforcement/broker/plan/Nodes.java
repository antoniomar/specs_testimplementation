/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specs.project.enforcement.broker.plan;

import java.util.List;
/**
 * contain information about a node created on the provider, and the recipes executed on it
 */
public class Nodes{
   	private boolean acquire_public_ip;
   	private List annotations;
   	private String appliance;
   	private String creation_time;
   	private Firewall firewall;
   	private String hardware;
   	private String node_id;
   	private List private_ips;
   	private Number private_ips_count;
   	private String provider;
   	private String public_ip;
   	private List recipes;
   	private List storages;

 	public boolean getAcquire_public_ip(){
		return this.acquire_public_ip;
	}
	public void setAcquire_public_ip(boolean acquire_public_ip){
		this.acquire_public_ip = acquire_public_ip;
	}
 	public List getAnnotations(){
		return this.annotations;
	}
	public void setAnnotations(List annotations){
		this.annotations = annotations;
	}
 	public String getAppliance(){
		return this.appliance;
	}
	public void setAppliance(String appliance){
		this.appliance = appliance;
	}
 	public String getCreation_time(){
		return this.creation_time;
	}
	public void setCreation_time(String creation_time){
		this.creation_time = creation_time;
	}
 	public Firewall getFirewall(){
		return this.firewall;
	}
	public void setFirewall(Firewall firewall){
		this.firewall = firewall;
	}
 	public String getHardware(){
		return this.hardware;
	}
	public void setHardware(String hardware){
		this.hardware = hardware;
	}
 	public String getNode_id(){
		return this.node_id;
	}
	public void setNode_id(String node_id){
		this.node_id = node_id;
	}
 	public List getPrivate_ips(){
		return this.private_ips;
	}
	public void setPrivate_ips(List private_ips){
		this.private_ips = private_ips;
	}
 	public Number getPrivate_ips_count(){
		return this.private_ips_count;
	}
	public void setPrivate_ips_count(Number private_ips_count){
		this.private_ips_count = private_ips_count;
	}
 	public String getProvider(){
		return this.provider;
	}
	public void setProvider(String provider){
		this.provider = provider;
	}
 	public String getPublic_ip(){
		return this.public_ip;
	}
	public void setPublic_ip(String public_ip){
		this.public_ip = public_ip;
	}
 	public List getRecipes(){
		return this.recipes;
	}
	public void setRecipes(List recipes){
		this.recipes = recipes;
	}
 	public List getStorages(){
		return this.storages;
	}
	public void setStorages(List storages){
		this.storages = storages;
	}
}
