/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Implement the funcionalities to create and manage the nodes in a Cloud Service Provide
using the jcloud library

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specs.project.enforcement.broker;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.jclouds.blobstore.domain.Blob;

import eu.specs.project.enforcement.broker.entities.credentials.ProviderCredential;

public class StorageService extends StorageServiceS3{

	//TODO: add class destructor and close the connection : context.close()
	
	/**
	 * Represents a Cloud Service Provider, it give access to creation and management of nodes.
	 * It uses jcloud to provide service over different cloud platforms
	 * @param provider identifier of the provider 
	 * @param defaultUser 
	 * @param providerCredential
	 */
	public StorageService(String provider, ProviderCredential providerCredential){
	    super( providerCredential);
		
	}
	
	

	public boolean createContainer(String container) {	
		try{
		//TODO: add the possibilities to chose the location using the Location object
		return super.createContainerS3( container, "us-west-1", false, false);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean createContainer(String container, String location ) {	
		try{
		//TODO: add the possibilities to chose the location using the Location object
		return super.createContainerS3( container, location, false, false);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean createContainer(String container, String location, Boolean versioning, Boolean createIAMUser) {	
		try{
		//TODO: add the possibilities to chose the location using the Location object
		return super.createContainerS3( container, location, versioning, createIAMUser);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean createContainer(String container, String location, String redundantLocation, Boolean createIAMUser) {	
		try{
		//TODO: add the possibilities to chose the location using the Location object
		return super.createContainerS3(container, location, redundantLocation, createIAMUser);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	
	public void deleteContainer(String container) {	
		try{
			//delete the container and all the related directories and files 
			super.deleteBucketS3(container);
		
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	
	public Blob uploadBlob(String containerName, String blobName, byte[] dataToStore ){
		try {
			InputStream byteDataToStore = new ByteArrayInputStream(dataToStore);
			
	        super.uploadBlobS3(blobName, byteDataToStore, dataToStore.length );
	        return null;
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public byte[] downloadBlob(String containerName, String blobName ){
		try {
       
			// Get a Blob
			return super.downloadBlobS3(containerName, blobName);
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public byte[] deleteBlob(String containerName, String blobName ){
		try {
			return super.deleteBlobS3(containerName, blobName);

		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void createDirectory(String containerName, String directoryName ){
		try {
			super.createDirectoryS3(containerName, directoryName);
			//blobStore.createDirectory(containerName, directoryName);
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	public void deleteDirectory(String containerName, String directoryName ){
		try {
			super.deleteDirectoryS3(containerName, directoryName);
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	

	

	
	
}
