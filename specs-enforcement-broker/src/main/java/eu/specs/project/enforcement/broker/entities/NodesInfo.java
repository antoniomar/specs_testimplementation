/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specs.project.enforcement.broker.entities;

import java.util.ArrayList;
import java.util.List;

import eu.specs.project.enforcement.broker.ClusterNode;
/**
 * contain all the information about the nodes created on a Cloud Service Provider
 * 
 *
 */
public class NodesInfo{
	private String rootPassword;
	private String privateKey;
	private String publicKey;
	private List<ClusterNode> nodes;
	
	/**
	 * create the class and instantiate an array list that will contain all the nodes @see ClusterNode
	 */
	public NodesInfo() {
		this.nodes = new ArrayList<ClusterNode>();
	}

	/**
	 * get the password of the root user
	 * @return string containing the user root password
	 */
	public String getRootPassword() {
		return rootPassword;
	}

	/**
	 * set the password of the root user 
	 * @param rootPassword string that contain the root password
	 */
	public void setRootPassword(String rootPassword) {
		this.rootPassword = rootPassword;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public List<ClusterNode> getNodes() {
		return nodes;
	}

	public void setNodes(List<ClusterNode> nodes) {
		this.nodes = nodes;
	}
	
	public void addNode(ClusterNode node){
		this.nodes.add(node);
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	
	
	
}