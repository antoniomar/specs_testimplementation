
package eu.specs.project.enforcement.broker.s3utils;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "Action",
    "Effect",
    "Resource"
})
public class S3Statement {

    @JsonProperty("Action")
    private List<String> Action = new ArrayList<String>();
    @JsonProperty("Effect")
    private String Effect;
    @JsonProperty("Resource")
    private List<String> Resource;

    /**
     * 
     * @return
     *     The Action
     */
    @JsonProperty("Action")
    public List<String> getAction() {
        return Action;
    }

    /**
     * 
     * @param Action
     *     The Action
     */
    @JsonProperty("Action")
    public void setAction(List<String> Action) {
        this.Action = Action;
    }

    /**
     * 
     * @return
     *     The Effect
     */
    @JsonProperty("Effect")
    public String getEffect() {
        return Effect;
    }

    /**
     * 
     * @param Effect
     *     The Effect
     */
    @JsonProperty("Effect")
    public void setEffect(String Effect) {
        this.Effect = Effect;
    }

    /**
     * 
     * @return
     *     The Resource
     */
    @JsonProperty("Resource")
    public List<String> getResource() {
        return Resource;
    }

    /**
     * 
     * @param Resource
     *     The Resource
     */
    @JsonProperty("Resource")
    public void setResource(List<String> Resource) {
        this.Resource = Resource;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
