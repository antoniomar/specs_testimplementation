package eu.specs.project.enforcement.broker.entities;

public class MetricInfo {
	
	public String id;
	public String name;
	public String agreed;
	public String cookbook;
	public String getCookbook() {
		return cookbook;
	}
	public void setCookbook(String cookbook) {
		this.cookbook = cookbook;
	}
	public String getMetricID() {
		return metricID;
	}
	public void setMetricID(String metricID) {
		this.metricID = metricID;
	}
	public String metricID;
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAgreed() {
		return agreed;
	}
	public void setAgreed(String agreed) {
		this.agreed = agreed;
	}
	
}