/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Implement the funcionalities to create and manage the nodes in a Cloud Service Provide
using the jcloud library

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specs.project.enforcement.broker;

import static org.jclouds.compute.config.ComputeServiceProperties.TIMEOUT_SCRIPT_COMPLETE;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;

import javax.sound.sampled.FloatControl.Type;

import org.jclouds.Constants;
import org.jclouds.ContextBuilder;
import org.jclouds.compute.ComputeService;
import org.jclouds.compute.ComputeServiceContext;
import org.jclouds.compute.domain.ExecResponse;
import org.jclouds.compute.domain.NodeMetadata;
import org.jclouds.domain.LoginCredentials;
import org.jclouds.openstack.keystone.v2_0.config.CredentialTypes;
import org.jclouds.openstack.keystone.v2_0.config.KeystoneProperties;
import org.jclouds.openstack.neutron.v2.NeutronApi;
import org.jclouds.openstack.neutron.v2.domain.FloatingIP;
import org.jclouds.openstack.neutron.v2.domain.FloatingIP.CreateFloatingIP;
import org.jclouds.openstack.neutron.v2.domain.FloatingIP.UpdateFloatingIP;
import org.jclouds.openstack.neutron.v2.domain.Network;
import org.jclouds.openstack.neutron.v2.domain.Network.CreateNetwork;
import org.jclouds.openstack.neutron.v2.domain.Port;
import org.jclouds.openstack.neutron.v2.domain.SecurityGroup;
import org.jclouds.openstack.neutron.v2.domain.SecurityGroup.CreateSecurityGroup;
import org.jclouds.openstack.neutron.v2.domain.Subnet;
import org.jclouds.openstack.neutron.v2.domain.Subnet.CreateSubnet;
import org.jclouds.openstack.neutron.v2.extensions.FloatingIPApi;
import org.jclouds.openstack.neutron.v2.extensions.SecurityGroupApi;
import org.jclouds.openstack.neutron.v2.features.NetworkApi;
import org.jclouds.openstack.neutron.v2.features.PortApi;
import org.jclouds.openstack.neutron.v2.features.SubnetApi;
import org.jclouds.openstack.nova.v2_0.NovaApi;
import org.jclouds.openstack.nova.v2_0.domain.Address;
import org.jclouds.openstack.nova.v2_0.domain.Flavor;
import org.jclouds.openstack.nova.v2_0.domain.Image;
import org.jclouds.openstack.nova.v2_0.domain.KeyPair;
import org.jclouds.openstack.nova.v2_0.domain.ServerCreated;
import org.jclouds.openstack.nova.v2_0.extensions.KeyPairApi;
import org.jclouds.openstack.nova.v2_0.features.FlavorApi;
import org.jclouds.openstack.nova.v2_0.features.ImageApi;
import org.jclouds.openstack.nova.v2_0.features.ServerApi;
import org.jclouds.openstack.nova.v2_0.options.CreateServerOptions;
import org.jclouds.openstack.v2_0.domain.Resource;
import org.jclouds.scriptbuilder.ScriptBuilder;
import org.jclouds.scriptbuilder.domain.LiteralStatement;
import org.jclouds.scriptbuilder.statements.ssh.AuthorizeRSAPublicKeys;
import org.jclouds.scriptbuilder.statements.ssh.InstallRSAPrivateKey;
import org.jclouds.ssh.jsch.config.JschSshClientModule;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Module;

import eu.specs.project.enforcement.broker.entities.InstanceDescriptor;
import eu.specs.project.enforcement.broker.entities.NodesInfo;
import eu.specs.project.enforcement.broker.entities.credentials.NodeCredential;
import eu.specs.project.enforcement.broker.entities.credentials.ProviderCredential;

public class CloudServiceOpenStack {
	private ComputeServiceContext context;
	private ComputeService compute;
	private final NovaApi novaApi;
	private NeutronApi neutronApi;
	private String defaultUser;
	private String defaultRegion;
	
	private Set<String> regions;

	private NodesInfo nodesInfo = new NodesInfo();

	private String networkId;
	private String networkName;
	private String publicNetwork;

	
	
	/**
	 * Represents a Cloud Service Provider, it give access to creation and management of nodes.
	 * It uses jcloud to provide service over different cloud platforms
	 * @param provider identifier of the provider 
	 * @param defaultUser 
	 * @param providerCredential
	 */
	public CloudServiceOpenStack(String provider, String defaultUser, ProviderCredential providerCredential){
		
		Properties overrides = new Properties();
		overrides.setProperty(KeystoneProperties.CREDENTIAL_TYPE, CredentialTypes.PASSWORD_CREDENTIALS);
		overrides.setProperty(Constants.PROPERTY_RELAX_HOSTNAME, "true");
		overrides.setProperty(Constants.PROPERTY_TRUST_ALL_CERTS, "true");
		
		novaApi = ContextBuilder.newBuilder("openstack-nova")
                .endpoint("http://192.168.27.100:5000/v2.0/")
                .credentials(providerCredential.getUsername(), providerCredential.getPassword())
                .overrides(overrides)
                .modules(ImmutableSet.<Module> of(new JschSshClientModule()))
                .buildApi(NovaApi.class);
		
        //instance for making request to the neutron component "network component"
        neutronApi = ContextBuilder.newBuilder("openstack-neutron")
                .endpoint("http://192.168.27.100:5000/v2.0/")
                .credentials(providerCredential.getUsername(), providerCredential.getPassword())
                //.modules(modules)
                .overrides(overrides)
                .buildApi(NeutronApi.class);
		
        regions = novaApi.getConfiguredRegions();
        
		Properties properties = new Properties();
	    long scriptTimeout = TimeUnit.MILLISECONDS.convert(20, TimeUnit.MINUTES);
	    properties.setProperty(TIMEOUT_SCRIPT_COMPLETE, scriptTimeout + "");
	    this.defaultUser = defaultUser;
	    
	    context = ContextBuilder.newBuilder("openstack-nova")
	    		.endpoint("http://192.168.27.100:5000/v2.0/")
                .credentials(providerCredential.getUsername(), providerCredential.getPassword())
                .overrides(properties)
                .modules(ImmutableSet.<Module> of(
                		 new JschSshClientModule()))
                .buildView(ComputeServiceContext.class); 
	    compute = context.getComputeService();
        

		if (defaultRegion == null) {
			defaultRegion = regions.iterator().next();
		}

	    this.defaultUser = defaultUser;

	    this.listNetworks();
	    
	    
	}

	
	
	/**
	 * creates multiple nodes in a Cloud Service Provider
	 * @param groupName string that represent the group to which the machines are assigned 
	 * @param numberOfInstances number of instances to create 
	 * @param descriptor object that contain information about the appliance, hw and zone
	 * @param nodeCredential object containing root password private and public key
	 * @param inboudports inbound ports to open on the created machines 
	 * @return information about all the nodes created and the credentials
	 * @throws NoSuchElementException
	 * @throws Exception
	 */
	public NodesInfo createNodesInGroup(String groupName, int numberOfInstances, InstanceDescriptor descriptor, NodeCredential nodeCredential, String publicKey, int... inboudports)  throws NoSuchElementException, Exception {	
		try {
			
			Network network = this.getNetworkByName("private");
			networkName = network.getName();
			networkId = network.getId();
			deleteAllKeypairs();
			deleteAllGroupsExceptDefault();
			
			for (int i = 0; i<numberOfInstances; i++){
				KeyPair keyPair = this.createKeyPair(groupName+"Key"+i, publicKey);
				//SecurityGroup securityGroup = this.createSecurityGroup(groupName+"secgroup"+i, "The security Group of the Host "+i);
				//Network network = this.createNetwork(groupName+"Net");
				
				SecurityGroup securityGroup = getSecurityGroupByName("default");
				HashSet<String> securityGroupList = new HashSet<String>();
				securityGroupList.add(securityGroup.getName());
				
				//String keyPairName = this.createKeyPair(groupName+i, publicKey).getName();
				ServerCreated serverCreated = this.launchServerMachine(groupName+i, 
						descriptor.getImage(), 
						descriptor.getHardwareId(), 
						keyPair.getName(), 
						networkId, 
						securityGroupList, "");
				try {
					Thread.sleep(30000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				String privateAddress = getHostAddress(serverCreated.getId());
				
				//add a public ip to the node
				String floatingNetworkId = getFloatingNetworkId();
				String portId = getPortByIp(privateAddress).getId();
				

				String publicIp = this.associatePublicIP(privateAddress, portId, floatingNetworkId);
				nodesInfo.addNode(new ClusterNode(serverCreated.getId(), publicIp, privateAddress));
				
			}
			
			
		}catch(NoSuchElementException e){
			throw new NoSuchElementException(e.getMessage());
		}
		catch (Exception e) {
			//destroyClusterWithName(groupName);
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		
		
					


//		HashSet<String> pubKeys = new HashSet<String>();
//		pubKeys.add(nodeCredential.getPublickey());
//		AuthorizeRSAPublicKeys authKeys = new AuthorizeRSAPublicKeys(pubKeys);
//		InstallRSAPrivateKey instPriv = new InstallRSAPrivateKey(nodeCredential.getPrivatekey());
		
		ScriptBuilder sb = new ScriptBuilder();
//		return nodesInfo;
		return nodesInfo;

//		sb.addStatement(authKeys);
//		sb.addStatement(instPriv);
//		ExecResponse er = null;
//		for(NodeMetadata n : nodes){
//			er = compute.runScriptOnNode(n.getId(), sb, 
//				overrideLoginCredentials(getLoginForCommandExecution(this.defaultUser, n.getCredentials().credential.trim())).runAsRoot(false).wrapInInitScript(false));
//			if(er.getExitStatus()!=0){
//				destroyClusterWithName(groupName);
//				throw new Exception("Error in configuration of custom RSA keys");
//			}
//		}
		
//		ExecResponse er = null;
//		List<ClusterNode> nodes = nodesInfo.getNodes();
//		
//		LiteralStatement pass = new LiteralStatement(String.format("echo -e \"%s\n%s\n\" | passwd root", nodeCredential.getRootpassword(),nodeCredential.getRootpassword()));
//		sb = new ScriptBuilder();
//		sb.addStatement(pass);
//		for(ClusterNode n : nodes){
//			er = compute.runScriptOnNode(n.getId(), sb, 
//				overrideLoginCredentials(getLoginForCommandExecution(defaultUser, n.getCredentials().credential.trim())).runAsRoot(true).wrapInInitScript(false));
//			if(er.getExitStatus()!=0){
//				destroyClusterWithName(groupName);
//				throw new Exception("Error in installing root password");
//			}
//		}
//		info.setPublicKey(nodeCredential.getPublickey());
//		info.setPrivateKey(nodeCredential.getPrivatekey());
//		info.setRootPassword(nodeCredential.getRootpassword());
//		return info;
	}	
	
//	public void destroyClusterWithName(String groupName){
//		try{
//			compute.destroyNodesMatching(and(inGroup(groupName), not(TERMINATED)));
//		} catch(Exception e){
//			e.getMessage();
//		}
//	}
	
	private LoginCredentials getLoginForCommandExecution(String user, String credential) {
		
	    return LoginCredentials.builder().user(user).credential(credential).build();   
	}
	
	//############################ Part related only to OpenStack 
	public String associateFloatingIP(String fixedIpAddress, String portId) {
		List < FloatingIP > freeIP = new LinkedList < FloatingIP > ();
		FloatingIPApi floatingIPApi = this.neutronApi.getFloatingIPApi("RegionOne").get();
		List <? extends FloatingIP > floatingIPList = floatingIPApi.list().concat().toList();

		System.out.println("Floating IP : ");
		for (FloatingIP floatingIP: floatingIPList){
			System.out.println(floatingIP.toString());
			//fixedIpAddress and portId are null if the address is not assigned
			if (floatingIP.getPortId() == null) {
				UpdateFloatingIP updateFloatingIP = FloatingIP.updateBuilder().portId(portId).fixedIpAddress(fixedIpAddress).build();
				floatingIPApi.update(floatingIP.getId(), updateFloatingIP);
				freeIP.add(floatingIP);
				
			}
		}
		
		if (freeIP.size() > 0) {
			return freeIP.get(0).getFloatingIpAddress();
			
		} else {
			CreateFloatingIP createFloatingIp = CreateFloatingIP.createBuilder(floatingIPList.get(0).getFloatingNetworkId()).build();
			return floatingIPApi.create(createFloatingIp).getFloatingIpAddress();
		}
	}
	
	public String associatePublicIP(String nodePrivateAddress, String nodePortId, String floatingNetworkId) {
		FloatingIPApi floatingIPApi = this.neutronApi.getFloatingIPApi("RegionOne").get();
		List <? extends FloatingIP > floatingIPList = floatingIPApi.list().concat().toList();

//		Port port = this.getPortByIp(nodePrivateAddress);
//		String portId = port.getId();
		
		CreateFloatingIP createFloatingIp = CreateFloatingIP.createBuilder(floatingNetworkId)
				.fixedIpAddress(nodePrivateAddress).portId(nodePortId).build();
		return floatingIPApi.create(createFloatingIp).getFloatingIpAddress();

	}
	

	
	private Network getNetworkByName(String networkName) {
		NetworkApi networkApi = neutronApi.getNetworkApi("RegionOne");
	    List<? extends Network> networks = networkApi.list().concat().toList();
	    String name;
	    CharSequence cs;
	    for (Network network: networks) {
	    	name = network.getName();
	    	cs = name;
	    	if (name.contentEquals(cs)){
	    		return network;
	    	}
	    }
	    return null;
	}
	
	private Port getPortByIp(String hostIp) {
		PortApi portApi = this.neutronApi.getPortApi("RegionOne");
		List < Port > portList = portApi.list().concat().toList();
	    String comparedIp;
	    for (Port port: portList) {
	    	comparedIp = port.getFixedIps().iterator().next().getIpAddress();
	    	if (hostIp.equalsIgnoreCase(comparedIp)){
	    		return port;
	    	}
	    }
	    return null;
	}

	public Network createNetwork(String name) {
		NetworkApi networkApi = neutronApi.getNetworkApi("RegionOne");
		CreateNetwork createNetworkOptions = CreateNetwork.createBuilder("newtestnet").build();
		Network network = networkApi.create(createNetworkOptions);
		return network;
	}
	
	public KeyPair createKeyPair(String name, String publickey) {
		KeyPairApi keyPairApi = novaApi.getKeyPairApi("RegionOne").get();
		KeyPair keyPair = keyPairApi.createWithPublicKey(name, publickey);
		
		return keyPair;
	}
	
	public Subnet createSubnet(String network, String cidr) {
		SubnetApi subnetApi = neutronApi.getSubnetApi("RegionOne");
		CreateSubnet createSubnetOptions = CreateSubnet.createBuilder(network, cidr).build();
		Subnet subnet = subnetApi.create(createSubnetOptions);
		//TODO: what is the number 4 in this sample? Subnet subnet = subnetApi.create(network, 4, cidr);
		return subnet;
	}
	
	private String getFloatingNetworkId() {
		NetworkApi networkApi = neutronApi.getNetworkApi("RegionOne");
	    List<? extends Network> networks = networkApi.list().concat().toList();
	    
	    for (Network network: networks) {
	    	System.out.println("    %s%n" + network);
	    	if (network.getExternal()){
	    		return network.getId();
	    	}
	    }
		return defaultUser;
	}

	public SecurityGroup createSecurityGroup(String name, String description) {
		SecurityGroupApi securityGroupApi = neutronApi.getSecurityGroupApi("RegionOne").get();
		CreateSecurityGroup createSecurityGroup = CreateSecurityGroup.createBuilder().name(name).description(description).tenantId("admin").build();
		SecurityGroup securityGroup = securityGroupApi.create(createSecurityGroup);
		ImmutableList<SecurityGroup> securityGroups = securityGroupApi.listSecurityGroups().concat().toList();
		for (SecurityGroup securityGroupElement : securityGroups){
			System.out.println("Sec Group Id : " + securityGroupElement.getId());
			System.out.println("Sec Group Name : " + securityGroupElement.getName());
		}
		return securityGroup;
	}
	
	public ServerCreated launchServerMachine(String name, String imageId, String flavorId, String keypair, String network, Iterable < String > securityGroupNames, String userData) {
		ServerApi serverApi = this.novaApi.getServerApi("RegionOne");
		CreateServerOptions options = CreateServerOptions.Builder.keyPairName(keypair).networks(network).securityGroupNames(securityGroupNames);
		ServerCreated serverCreated = serverApi.create(name, imageId, flavorId, options);
		return serverCreated;
	}
	

	
//	public void attachIP(String ip, String server) {
//		UpdateFloatingIP updateFloatingIP = FloatingIP.updateBuilder().portId("").build();
//		floatingIPApi.update("", updateFloatingIP);
//
//
//	}
	
	private void listHardware(){
		FlavorApi flavorApi = this.novaApi.getFlavorApi(this.defaultRegion);
		ImmutableList<Flavor> flavorList = flavorApi.listInDetail().concat().toList();
		System.out.println("Flavor List");
		for (Resource flavor : flavorList){
			System.out.println("Image Id : " + flavor.getId());
			System.out.println("Image Name : " + flavor.getName());
		}
	}
	
//	private void listLocations(){
//		AvailabilityZoneApi zoneApi = this.novaApi.getAvailabilityZoneApi(this.defaultRegion).get();
//		FluentIterable<AvailabilityZone> zoneList = zoneApi.listAvailabilityZones();
//		for (AvailabilityZone zone : zoneList){
//			zone.
//		}
//		
//	}
	
	private void listImages(){
		ImageApi imageApi = this.novaApi.getImageApi(this.defaultRegion);

		ImmutableList<Image> iamgeList = imageApi.listInDetail().concat().toList();
		System.out.println("Image List");
		for (Resource image : iamgeList){
			System.out.println("Image Id : " + image.getId());
			System.out.println("Image Name : " + image.getName());
		}

	}
	
	private void listNetworks() {
		NetworkApi networkApi = neutronApi.getNetworkApi("RegionOne");
	    List<? extends Network> networks = networkApi.list().concat().toList();
	    System.out.println("  Networks: %n");
	    for (Network network: networks) {
	    	
	    	System.out.println("    %s%n"+ network.toString());
	    }
	}
	
	private SecurityGroup getSecurityGroupByName(String securityGroupName){
		SecurityGroupApi securityGroupApi = neutronApi.getSecurityGroupApi(this.defaultRegion).get();
		try {
			ImmutableList<SecurityGroup> securityGroupList = securityGroupApi.listSecurityGroups().concat().toList();
			for (SecurityGroup securityGroup : securityGroupList){
				if (securityGroup.getName().equalsIgnoreCase(securityGroupName)){
					return securityGroup;
				}
			}
			return null;
		} catch (Exception e) {
			ImmutableList<SecurityGroup> securityGroupList = securityGroupApi.listSecurityGroups().concat().toList();
			System.out.println("Security group list");
			for (SecurityGroup securityGroup : securityGroupList) {
				System.out.println("Id : "+ securityGroup.getId());
				System.out.println("Id : "+ securityGroup.getName());
			}
			return null;
		}

	}
	
	private String getHostAddress(String serverId){
		ServerApi serverApi = this.novaApi.getServerApi("RegionOne");
		Collection<Entry<String, Address>> addressesIterator = serverApi.get(serverId).getAddresses().entries();
		
		System.out.println("Address List ");
		String privateAddress = null;
		for (Entry<String, Address> addressEntity : addressesIterator) {
			System.out.println("Address key : "+ addressEntity.getKey());
			System.out.println("Address key : "+ addressEntity.getValue());
			if (addressEntity.getKey().equalsIgnoreCase("private"))	{
				privateAddress = addressEntity.getValue().getAddr();
			}
		
		}
		return privateAddress;

	}
	
	private void deleteAllKeypairs()	{
		KeyPairApi keyPairApi = novaApi.getKeyPairApi("RegionOne").get();
		Iterator<KeyPair> keyPairIterator = keyPairApi.list().iterator();
		while (keyPairIterator.hasNext()){
			
			String keyPairName = keyPairIterator.next().getName();
			keyPairApi.delete(keyPairName);
			System.out.println("Deleted key : " + keyPairName);
			
	    }

	}
	
	private void deleteAllGroupsExceptDefault()	{
		SecurityGroupApi securityGroupApi = neutronApi.getSecurityGroupApi("RegionOne").get();
		ImmutableList<SecurityGroup> securityGroups = securityGroupApi.listSecurityGroups().concat().toList();
		for (SecurityGroup securityGroupElement : securityGroups){
			if (!securityGroupElement.getName().equalsIgnoreCase("default")){
				System.out.println("Delete Security Group : " + securityGroupElement.getName());
				securityGroupApi.deleteSecurityGroup(securityGroupElement.getId());
			}
		}
	}
	
	
}






