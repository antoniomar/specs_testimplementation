package eu.specs.project.enforcement.broker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

import eu.specs.project.enforcement.broker.entities.credentials.NodeCredential;
import eu.specs.project.enforcement.broker.entities.credentials.ProviderCredential;

public class InfoManager {

	private static String hecaIP;
	private static String hecaPort;
	private static String chefUserUsername;
	private static String chefEndpoint;
	private static String chefServerUsername;
	private static String chefOrganizationPK;
	private static String chefUserPK;


	public InfoManager(String resourceFolder, String propertyFileName){

		Properties prop = new Properties();
		try{
			prop.load(Thread.currentThread().getContextClassLoader().
					getResourceAsStream(resourceFolder+propertyFileName));
			
			//BROKER PROVIDER CREDENTIALS
			//property: broker.provider_credentials.dir
			//servlet context attribute: not in servlet context	
			
			URL brokerCredentialsDirectoryUrl = Thread.currentThread().getContextClassLoader().getResource(resourceFolder + prop.getProperty("broker.provider_credentials.dir"));
			File brokerCredentialsDirectory = new File(brokerCredentialsDirectoryUrl.toURI());

			for ( File f : brokerCredentialsDirectory.listFiles())  {

				Properties providerCredProp = new Properties();
				providerCredProp.load(new FileInputStream(f));

				ProviderCredential cred = new ProviderCredential(providerCredProp.getProperty("username"), 
						providerCredProp.getProperty("password"));

				ProviderCredentialsManager.add( providerCredProp.getProperty("provider") , cred);
			}			
			/////
			
			//BROKER NODES CREDENTIALS
			//property: broker.node_credentials.rootpassword
			//			broker.node_credentials.public_key
			//			broker.node_credentials.private_key 
			//servlet context attribute: not in servlet context	
		
			File privateKeyfile = new File(Thread.currentThread().getContextClassLoader().getResource(resourceFolder + prop.getProperty("broker.node_credentials.private_key")).toURI());
			File publicKeyfile = new File(Thread.currentThread().getContextClassLoader().getResource(resourceFolder + prop.getProperty("broker.node_credentials.public_key")).toURI());
			
			NodeCredential nodeCred = new NodeCredential(prop.getProperty("broker.node_credentials.rootpassword"), 
					new String (Files.readAllBytes(Paths.get(publicKeyfile.getAbsolutePath()))), 
					new String  (Files.readAllBytes(Paths.get(privateKeyfile.getAbsolutePath()))));
			NodeCredentialsManager.add("def", nodeCred);						
			
			//CHEF
			//property: chef.conf
			//servlet context attribute: 
			//		chefUserUsername
			//		chefEndpoint
			//		chefServerUsername
			//		chefOrganizationPK
			//		chefUserPK
					
			Properties chefProp = new Properties();
			chefProp.load(Thread.currentThread().getContextClassLoader().
					getResourceAsStream(resourceFolder+prop.getProperty("chef.conf")));
	
			chefUserUsername = chefProp.getProperty("chef.user");
			chefEndpoint = chefProp.getProperty("chef.server.endpoint");
			chefServerUsername = chefProp.getProperty("chef.organization");		
		 
			String line;
			chefUserPK="";
			BufferedReader br = new BufferedReader(new InputStreamReader (Thread.currentThread().getContextClassLoader().getResourceAsStream(resourceFolder + chefProp.getProperty("chef.user.privatekey")))); 
			while (( line = br.readLine()) != null) {
				chefUserPK+=(line+"\n");
			}
			
			br.close();
			
			chefOrganizationPK="";
			br = new BufferedReader(new InputStreamReader ( Thread.currentThread().getContextClassLoader().getResourceAsStream(resourceFolder + chefProp.getProperty("chef.organization.privatekey"))));
			while (( line = br.readLine()) != null) {
				chefOrganizationPK+=(line+"\n");
			}
			br.close();
		
		
			//################
			//HECA
			hecaIP = prop.getProperty("heca.ip");
			hecaPort = prop.getProperty("heca.port");		
			
			}catch (Exception excp){
				//TODO better??
				excp.printStackTrace();
			}
		
		
		
				
	}

	
	public static String getHecaIP() {
		return hecaIP;
	}
	
	
	
	public static String getHecaPort() {
		return hecaPort;
	}
	
	
	
	public static String getChefUserUsername() {
		return chefUserUsername;
	}
	
	
	
	public static String getChefEndpoint() {
		return chefEndpoint;
	}
	
	
	
	public static String getChefServerUsername() {
		return chefServerUsername;
	}
	
	
	
	public static String getChefOrganizationPK() {
		return chefOrganizationPK;
	}
	
	
	
	public static String getChefUserPK() {
		return chefUserPK;
	}

	public static ProviderCredential getProviderCredentials(String provider) {
		return ProviderCredentialsManager.getCredentials(provider);
	}
	
	public static NodeCredential getNodeCredentials(String provider) {
		return NodeCredentialsManager.getCredentials(provider);
	}
		
	
	
}
