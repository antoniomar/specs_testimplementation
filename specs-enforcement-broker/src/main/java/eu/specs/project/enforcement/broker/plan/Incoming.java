/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specs.project.enforcement.broker.plan;

import java.util.List;

public class Incoming{
   	private String networkInterface;
   	private String port_list;
   	private String proto;
   	private String source_ips;
   	private List source_nodes;

 	public String getnetworkInterface(){
		return this.networkInterface;
	}
	public void setnetworkInterface(String networkInterface){
		this.networkInterface = networkInterface;
	}
 	public String getPort_list(){
		return this.port_list;
	}
	public void setPort_list(String port_list){
		this.port_list = port_list;
	}
 	public String getProto(){
		return this.proto;
	}
	public void setProto(String proto){
		this.proto = proto;
	}
 	public String getSource_ips(){
		return this.source_ips;
	}
	public void setSource_ips(String source_ips){
		this.source_ips = source_ips;
	}
 	public List getSource_nodes(){
		return this.source_nodes;
	}
	public void setSource_nodes(List source_nodes){
		this.source_nodes = source_nodes;
	}
}
