
package eu.specs.project.enforcement.broker.s3utils;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "Version",
    "Statement"
})
public class S3Policy {

    @JsonProperty("Version")
    private String Version;
    @JsonProperty("Statement")
    private List<S3Statement> Statement = new ArrayList<S3Statement>();

    /**
     * 
     * @return
     *     The Version
     */
    @JsonProperty("Version")
    public String getVersion() {
        return Version;
    }

    /**
     * 
     * @param Version
     *     The Version
     */
    @JsonProperty("Version")
    public void setVersion(String Version) {
        this.Version = Version;
    }

    /**
     * 
     * @return
     *     The Statement
     */
    @JsonProperty("Statement")
    public List<S3Statement> getStatement() {
        return Statement;
    }

    /**
     * 
     * @param Statement
     *     The Statement
     */
    @JsonProperty("Statement")
    public void setStatement(List<S3Statement> Statement) {
        this.Statement = Statement;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
