/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specs.project.enforcement.broker.plan;

/**
 * Contain a reference to a recipe 
 * @see Nodes
 */
public class Recipes{
   	private String cookbook;
   	private String id;
   	private Number implementation_phase;
   	private String name;

 	public String getCookbook(){
		return this.cookbook;
	}
	public void setCookbook(String cookbook){
		this.cookbook = cookbook;
	}
 	public String getId(){
		return this.id;
	}
	public void setId(String id){
		this.id = id;
	}
 	public Number getImplementation_phase(){
		return this.implementation_phase;
	}
	public void setImplementation_phase(Number implementation_phase){
		this.implementation_phase = implementation_phase;
	}
 	public String getName(){
		return this.name;
	}
	public void setName(String name){
		this.name = name;
	}
}
