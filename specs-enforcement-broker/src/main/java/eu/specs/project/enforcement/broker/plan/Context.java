/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specs.project.enforcement.broker.plan;

import java.util.List;

public class Context{
   	private List annotations;
   	private String monitoring_core_ip;
   	private String monitoring_core_port;
   	private String planID;
   	private String slaID;

 	public List getAnnotations(){
		return this.annotations;
	}
	public void setAnnotations(List annotations){
		this.annotations = annotations;
	}
 	public String getMonitoring_core_ip(){
		return this.monitoring_core_ip;
	}
	public void setMonitoring_core_ip(String monitoring_core_ip){
		this.monitoring_core_ip = monitoring_core_ip;
	}
 	public String getMonitoring_core_port(){
		return this.monitoring_core_port;
	}
	public void setMonitoring_core_port(String monitoring_core_port){
		this.monitoring_core_port = monitoring_core_port;
	}
 	public String getPlanID(){
		return this.planID;
	}
	public void setPlanID(String planID){
		this.planID = planID;
	}
 	public String getSlaID(){
		return this.slaID;
	}
	public void setSlaID(String slaID){
		this.slaID = slaID;
	}
}
