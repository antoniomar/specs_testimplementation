/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Implement the funcionalities to create and manage the nodes in a Cloud Service Provide
using the jcloud library

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specs.project.enforcement.broker;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.policy.Policy;
import com.amazonaws.auth.policy.Statement;
import com.amazonaws.auth.policy.Statement.Effect;
import com.amazonaws.auth.policy.actions.S3Actions;
import com.amazonaws.auth.policy.resources.S3ObjectResource;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClient;
import com.amazonaws.services.identitymanagement.model.AccessKey;
import com.amazonaws.services.identitymanagement.model.AccessKeyMetadata;
import com.amazonaws.services.identitymanagement.model.CreateAccessKeyRequest;
import com.amazonaws.services.identitymanagement.model.CreateLoginProfileRequest;
import com.amazonaws.services.identitymanagement.model.CreateRoleRequest;
import com.amazonaws.services.identitymanagement.model.CreateUserRequest;
import com.amazonaws.services.identitymanagement.model.DeleteAccessKeyRequest;
import com.amazonaws.services.identitymanagement.model.DeleteLoginProfileRequest;
import com.amazonaws.services.identitymanagement.model.DeleteRolePolicyRequest;
import com.amazonaws.services.identitymanagement.model.DeleteRoleRequest;
import com.amazonaws.services.identitymanagement.model.DeleteUserPolicyRequest;
import com.amazonaws.services.identitymanagement.model.DeleteUserRequest;
import com.amazonaws.services.identitymanagement.model.GetRoleRequest;
import com.amazonaws.services.identitymanagement.model.GetRoleResult;
import com.amazonaws.services.identitymanagement.model.ListAccessKeysRequest;
import com.amazonaws.services.identitymanagement.model.ListRolePoliciesRequest;
import com.amazonaws.services.identitymanagement.model.ListUserPoliciesRequest;
import com.amazonaws.services.identitymanagement.model.ListUsersRequest;
import com.amazonaws.services.identitymanagement.model.NoSuchEntityException;
import com.amazonaws.services.identitymanagement.model.PutRolePolicyRequest;
import com.amazonaws.services.identitymanagement.model.PutUserPolicyRequest;
import com.amazonaws.services.identitymanagement.model.Role;
import com.amazonaws.services.identitymanagement.model.User;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.BucketReplicationConfiguration;
import com.amazonaws.services.s3.model.BucketVersioningConfiguration;
import com.amazonaws.services.s3.model.GetBucketLocationRequest;
import com.amazonaws.services.s3.model.ListVersionsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.ReplicationDestinationConfig;
import com.amazonaws.services.s3.model.ReplicationRule;
import com.amazonaws.services.s3.model.ReplicationRuleStatus;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.model.S3VersionSummary;
import com.amazonaws.services.s3.model.SetBucketVersioningConfigurationRequest;
import com.amazonaws.services.s3.model.Region;
import com.amazonaws.services.s3.model.VersionListing;
import com.amazonaws.util.IOUtils;
import com.google.gson.Gson;

import eu.specs.project.enforcement.broker.entities.credentials.ProviderCredential;
import eu.specs.project.enforcement.broker.s3utils.S3Policy;
import eu.specs.project.enforcement.broker.s3utils.S3Statement;


public class StorageServiceS3 {

	private String bucketName;
    private String replicaBucketName;
    private String firstRegion;
    private String redundantRegion;
    //username and password of the new created user for accessing Amazon AWS Console
    private String usernameIAM;
    private String passwordIAM;
    //access key id and secret access key for using the AWS API with the new created user
    private AccessKey accessKey;
    private BasicAWSCredentials awsCreds;
    //IAM manager used to generate policy and users 
    private AmazonIdentityManagementClient iamManager;
    //object that has the rights for creating buckets
    private AmazonS3 s3ClientMgr;
    //object that has the rights for using the generated buckets
    private AmazonS3 s3ClientUser;
    
    /**
     * Create a iamManager instance to generate credentials
     * Create an AmazonS3Client instance to do the administrator operations. 
     * @param provider
     * @param providerCredential
     */
    public StorageServiceS3(ProviderCredential providerCredential){
		//get the administrator credentials (need rights for creating users and roles)
		this.awsCreds = new BasicAWSCredentials(providerCredential.getUsername(), providerCredential.getPassword());
		this.iamManager = new AmazonIdentityManagementClient(this.awsCreds);
		this.s3ClientMgr = new AmazonS3Client(this.awsCreds);
		this.s3ClientUser = this.s3ClientMgr;
    	
    }
    
    
	/**
	 * Create a bucket on a specified region, It can enables the versioning and 
	 * can create a user with ability of create / delete / upload objects
	 * @param providerCredential credentials related to a user that can create IAM users and buckets
	 * @param bucketName name of the bucket to create 
	 * @param regionName name of the location in which the bucket will be created
	 * @param versioning enables the versioning giving the ability of restore objects
	 * @param createIAMUser specify if a new user must bu created, The new user can only upload download and edit objects
	 */
	//create a bucket on a specified region and can also enables the versioning, createIAMUse
	public boolean createContainerS3 (String bucketName, String regionName, Boolean versioning, Boolean createIAMUser) {
		
		this.bucketName = bucketName;
		this.replicaBucketName = null;
		this.firstRegion = regionName;
		this.redundantRegion = null;

		/*
		 * create the bucket on the specified location
		 */
        try {
            if((s3ClientMgr.doesBucketExist(this.bucketName)))
            {
            	if (!this.emptyAndDeleteBucketS3(this.bucketName)){
            		return false;}
            }
            
            this.s3ClientMgr.createBucket(this.bucketName, Region.fromValue(regionName));
            
            // Get location
            String bucketLocation = s3ClientMgr.getBucketLocation(new GetBucketLocationRequest(this.bucketName));
            System.out.println("bucket location = " + bucketLocation);

         } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which " +
            		"means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
            return false;
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which " +
            		"means the client encountered " +
                    "an internal error while trying to " +
                    "communicate with S3, " +
                    "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
            return false;
        }
        
        /*
         * Enable versioning on the bucket.
         */
        if (versioning == true){
            try {

                // Enable versioning on the bucket.
            	BucketVersioningConfiguration configuration = 
            			new BucketVersioningConfiguration().withStatus("Enabled");
                
    			SetBucketVersioningConfigurationRequest setBucketVersioningConfigurationRequest = 
    					new SetBucketVersioningConfigurationRequest(this.bucketName,configuration);
    			
    			s3ClientMgr.setBucketVersioningConfiguration(setBucketVersioningConfigurationRequest);
    			
    			// Verify bucket versioning configuration information.
    			BucketVersioningConfiguration conf = s3ClientMgr.getBucketVersioningConfiguration(this.bucketName);
    			 System.out.println("bucket versioning configuruation status:    " + conf.getStatus());

            } catch (AmazonS3Exception amazonS3Exception) {
                System.out.format("An Amazon S3 error occurred. Exception: %s", amazonS3Exception.toString());
                return false;
            } catch (Exception ex) {
                System.out.format("Exception: %s", ex.toString());
                return false;
            } 
            
        }
        
		/*
		 * create a new IAM user with the rights of using buckets
		 */
		if (createIAMUser){
			try{
				Statement allowOperationsOnFirstBucket = new Statement(Effect.Allow)
			    .withActions(S3Actions.GetObject, S3Actions.PutObject, S3Actions.DeleteObject)
			    .withResources(new S3ObjectResource(this.bucketName, "*"));

			    Policy policy = new Policy()
			        .withStatements(allowOperationsOnFirstBucket);
			    //create a new user with the policy defined before and save the credentials in usernameIAM , passwordIAM
				this.createIAMuser(policy);
			}catch (Exception e){
				e.printStackTrace();
				return false;
			}

		}
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;    
        
    }
	
	/**
	 * Create a bucket with versioning enabled on a specified region, enable the replication of each object on a different region
	 * It can also create a user with ability of get / put objects on the first bucket and to get objects from the redundant bucket
	 * @param providerCredential credentials related to a user that can create IAM users and buckets
	 * @param bucketName name of the bucket to create 
	 * @param regionName name of the location in which the first bucket will be created
	 * @param redundantRegionName name of the location in which the redundant bucket will be created
	 * @param createIAMUser specify if a new user must be created, The new user can only upload / download objects from the first bucket or download from the redundant bucket
	 */
	
	public boolean createContainerS3(String bucketName, String regionName, String redundantRegionName, Boolean createIAMUser) {
		
		this.bucketName = bucketName;
    	this.replicaBucketName = this.bucketName+"replica";
		this.firstRegion = regionName;
		this.redundantRegion = redundantRegionName;
        
		/*
		 * remove configurations on the buckets
		 */
		try{
			s3ClientMgr.deleteBucketReplicationConfiguration(this.bucketName);
		}catch(Exception e){
			System.out.println("There isn't a configureation for replication already set");
		}
		
        /*
         * create the first and the redundant bucket
         */
        try {
            if((this.s3ClientMgr.doesBucketExist(this.bucketName)))
            {
            	if (!this.emptyAndDeleteBucketS3(this.bucketName)){
            		return false;}
            
            }
            this.s3ClientMgr.createBucket(this.bucketName, Region.fromValue(this.firstRegion));
        	
        	if((this.s3ClientMgr.doesBucketExist(this.replicaBucketName)))
            {
        		if (!this.emptyAndDeleteBucketS3(this.replicaBucketName)){
        			return false;}
            }
        	
        	this.s3ClientMgr.createBucket(replicaBucketName, Region.fromValue(this.redundantRegion));
        	
            // Get location.
            String bucketLocation = this.s3ClientMgr.getBucketLocation(new GetBucketLocationRequest(this.bucketName));
            System.out.println("bucket location = " + bucketLocation);
            String bucketReplicaLocation = this.s3ClientMgr.getBucketLocation(new GetBucketLocationRequest(this.replicaBucketName));
            System.out.println("bucket replica location = " + bucketReplicaLocation);


         } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which " +
            		"means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
            return false;
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which " +
            		"means the client encountered " +
                    "an internal error while trying to " +
                    "communicate with S3, " +
                    "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
            return false;
        }
        
        /*
         * Enable versioning on the bucket.
         */
        try {

        	// Enable versioning on the main bucket.
        	BucketVersioningConfiguration configuration = new BucketVersioningConfiguration().withStatus("Enabled");
        	
        	if (this.s3ClientMgr.getBucketVersioningConfiguration(this.bucketName).getStatus() != "Enabled"){
        		SetBucketVersioningConfigurationRequest setBucketVersioningConfigurationRequest = 
    					new SetBucketVersioningConfigurationRequest(this.bucketName,configuration);
        		
        		this.s3ClientMgr.setBucketVersioningConfiguration(setBucketVersioningConfigurationRequest);
        		
        	}
        	
        	// Enable versioning on the replicated bucket.
        	if (this.s3ClientMgr.getBucketVersioningConfiguration(this.replicaBucketName).getStatus() != "Enabled"){
        		SetBucketVersioningConfigurationRequest setBucketVersioningConfigurationRequest = 
    					new SetBucketVersioningConfigurationRequest(this.replicaBucketName,configuration);
        		
        		this.s3ClientMgr.setBucketVersioningConfiguration(setBucketVersioningConfigurationRequest);
        	}
            
			
			// Verify bucket versioning configuration information.
			BucketVersioningConfiguration conf = this.s3ClientMgr.getBucketVersioningConfiguration(this.bucketName);
			System.out.println("bucket versioning configuruation status:    " + conf.getStatus());
			 
			conf = this.s3ClientMgr.getBucketVersioningConfiguration(this.replicaBucketName);
			System.out.println("bucket replication versioning configuruation status:    " + conf.getStatus());

        } catch (AmazonS3Exception amazonS3Exception) {
            System.out.format("An Amazon S3 error occurred. Exception: %s", amazonS3Exception.toString());
            return false;
        } catch (Exception ex) {
            System.out.format("Exception: %s", ex.toString());
            return false;
        } 

        /*
         * create the role that give to the amazon S3 server the rights for replicating the objects uploaded on the the first bucket
         */
        //verify if there is already a role with the same name and delete it
        try {
			this.deleteRoleIfExists("role"+this.bucketName);
	        //create the json policy to put on the role
	        String jsonS3Policy = this.createJSONPolicy();
	        System.out.println(jsonS3Policy);
			
	        //specify who will assume the role
	        String assumeRoleDoc = "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Sid\":\"\",\"Effect\":\"Allow\",\"Principal\":{\"Service\":\"s3.amazonaws.com\"},\"Action\":\"sts:AssumeRole\"}]}";
	        
	        //create a role that give to amazon the rights for replicating the objects
	        iamManager.createRole(new CreateRoleRequest()
	        .withRoleName("role"+this.bucketName)
	        .withAssumeRolePolicyDocument(assumeRoleDoc));
	        
	        //associate the role with the policy
	        iamManager.putRolePolicy(new PutRolePolicyRequest()
	        .withRoleName("role"+this.bucketName)
	        .withPolicyName("allow-s3-read")
	        .withPolicyDocument(jsonS3Policy));
	        
	        //get the role ARN related to the permissions of replication
	        GetRoleResult roleResult = iamManager.getRole(new GetRoleRequest().withRoleName("role"+this.bucketName));
	        String roleARN = roleResult.getRole().getArn();
	        
	        String destinationBucketArn = "arn:aws:s3:::"+replicaBucketName;

			/*
			 * define all the rules related to witch object must be replicated
			 */

            Map<String, ReplicationRule> replicationRules = new HashMap<String, ReplicationRule>();
			replicationRules.put(
                    "replicationrule"+this.bucketName,
                    new ReplicationRule()
                        .withPrefix("")
                        .withStatus(ReplicationRuleStatus.Enabled)
                        .withDestinationConfig(
                                new ReplicationDestinationConfig().withBucketARN(destinationBucketArn)
                        )
            );
			// set the configuration defined for activating the bucket replication  
			this.s3ClientMgr.setBucketReplicationConfiguration(
                    bucketName,
                    new BucketReplicationConfiguration()
                        .withRoleARN(roleARN)
                        .withRules(replicationRules)
            ); 
            
            //get the configuration related to replication
            BucketReplicationConfiguration replicationConfig = this.s3ClientMgr.getBucketReplicationConfiguration(bucketName);     
            ReplicationRule rule = replicationConfig.getRule("replicationrule"+this.bucketName);
            
            System.out.println("Destination Bucket ARN : " + rule.getDestinationConfig().getBucketARN());
            System.out.println("Prefix : " + rule.getPrefix());
            System.out.println("Status : " + rule.getStatus());
            
        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which" +
                    " means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
            return false;
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means"+
                    " the client encountered " +
                    "a serious internal problem while trying to " +
                    "communicate with Amazon S3, " +
                    "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
            return false;
        } catch (Exception e){
        	e.printStackTrace();
        	return false;
        }
        
		/*
		 * create a new IAM user with the rights of using buckets
		 */
		if (createIAMUser){
			try {
				//give to the new user the rights for get and put objects on the first bucket 
				Statement allowOperationsOnFirstBucket = new Statement(Effect.Allow)
				    .withActions(S3Actions.GetObject, S3Actions.PutObject, S3Actions.DeleteObject)
				    .withResources(new S3ObjectResource(this.bucketName, "*"));
				
				//give to the new user the rights for get objects on the replica bucket
				Statement allowReadOnRedundantBucket = new Statement(Effect.Allow)
				    .withActions(S3Actions.GetObject)
				    .withResources(new S3ObjectResource(this.replicaBucketName, "*"));
			
			    Policy policy = new Policy()
			        .withStatements(allowOperationsOnFirstBucket, allowReadOnRedundantBucket);
			    
			    //create a IAM user with the same name of the bucket, if the name already exist delete it first
				this.createIAMuser(policy);
			}catch (Exception e){
				e.printStackTrace();
				return false;
			}
		} 
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return true;       
    }
	
	
	public boolean uploadBlobS3(String blobName, InputStream fileToStore, long length ){
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(length);
		try{
			s3ClientUser.putObject(this.bucketName, blobName, fileToStore, metadata);		
			
		}catch(Exception e){
			return false;
		}
		return true;
	}
	
	public byte[] downloadBlobS3(String containerName, String blobName ){
		S3Object obj = s3ClientUser.getObject(containerName, blobName);
		S3ObjectInputStream s3obj = obj.getObjectContent();
		byte[] byteArray;
		try {
			byteArray = IOUtils.toByteArray(s3obj);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return byteArray;
	}
	
	public byte[] deleteBlobS3(String containerName, String blobName ){
		byte[] deletedBlob;
		try{
			deletedBlob = downloadBlobS3(containerName, blobName );
			s3ClientUser.deleteObject(containerName, blobName);
		}catch(Exception e){
			
			return null;
		}

		return deletedBlob;
	}
	
	public boolean createDirectoryS3(String containerName, String directoryName ){
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(0);
		// Create empty content
		InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
		// Create a PutObjectRequest passing the folder name suffixed by /
		PutObjectRequest putObjectRequest = new PutObjectRequest(containerName, directoryName + "/", emptyContent, metadata);
		try{
			s3ClientUser.putObject(putObjectRequest);
			
		}catch(Exception e){
			return false;
		}
		return true;
	}

	public void deleteDirectoryS3(String containerName, String directoryName ){
		try {
			//blobStore.deleteDirectory(containerName, directoryName);
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	

	public String getBucketName() {
		return bucketName;
	}

	public String getReplicaBucketName() {
		return replicaBucketName;
	}

	public String getFirstRegion() {
		return firstRegion;
	}

	public String getRedundantRegion() {
		return redundantRegion;
	}

	public String getUsernameIAM() {
		return usernameIAM;
	}

	public String getPasswordIAM() {
		return passwordIAM;
	}

	public String getAccessKeyId() {
		return this.accessKey.getAccessKeyId();
	}

	public String getSecretAccessKey() {
		return this.accessKey.getSecretAccessKey();
	}

	private String createJSONPolicy() throws Exception{
        /*
         * Create the policies that give the ability of replicate object
         */
        List<S3Statement> statementList = new ArrayList<S3Statement>();
                
        //create the first statement and add to the statement list
        S3Statement createdStatement1 = new S3Statement();
        List<String> actionList1 = new ArrayList<String>();
        actionList1.add("s3:GetReplicationConfiguration");
        actionList1.add("s3:ListBucket");
        createdStatement1.setAction(actionList1);
        createdStatement1.setEffect("Allow");
        List<String> resource1 = new ArrayList<String>();
        resource1.add("arn:aws:s3:::"+this.bucketName);
        createdStatement1.setResource(resource1);
        statementList.add(createdStatement1);
      
        //create the second statement and add to the statement list
        S3Statement createdStatement2 = new S3Statement();
        List<String> actionList2 = new ArrayList<String>();
        actionList2.add("s3:GetObjectVersion");
        actionList2.add("s3:GetObjectVersionAcl");
        createdStatement2.setAction(actionList2);
        createdStatement2.setEffect("Allow");
        List<String> resource2 = new ArrayList<String>();
        resource2.add("arn:aws:s3:::"+this.bucketName+"/*");
        createdStatement2.setResource(resource2);
        statementList.add(createdStatement2);
        
        //create the third statement and add to the statement list
        S3Statement createdStatement3 = new S3Statement();
        List<String> actionList3 = new ArrayList<String>();
        actionList3.add("s3:ReplicateObject");
        actionList3.add("s3:ReplicateDelete");
        createdStatement3.setAction(actionList3);
        createdStatement3.setEffect("Allow");
        List<String> resource3 = new ArrayList<String>();
        resource3.add("arn:aws:s3:::"+this.replicaBucketName+"/*");
        createdStatement3.setResource(resource3);
        statementList.add(createdStatement3);
        
        //create the whole policy
        S3Policy createdPolicy = new S3Policy();
        createdPolicy.setVersion("2012-10-17");
        createdPolicy.setStatement(statementList);
        
        Gson gson = new Gson();
		return gson.toJson(createdPolicy);
	}
	
	//generate a 32 Byte random string
	private String passwordGenerator(){
		SecureRandom random = new SecureRandom();
		return new BigInteger(130, random).toString(32);
		
	}
	
	private void createIAMuser(Policy policy) throws Exception{
	
    	// verify if there is a user with the same name and delete it
    	this.deleteUserIfExists(this.bucketName);

    	Thread.sleep(500);
    	
    	//create a new IAM user
    	System.out.println("Create the newuser : "+this.bucketName);
    	this.iamManager.createUser(new CreateUserRequest().withUserName(this.bucketName));
    	Thread.sleep(500);


        this.usernameIAM = this.bucketName;
        //generate a random password
        this.passwordIAM = this.passwordGenerator();

        //associate the generated password to the user
    	System.out.println("Create login profile for the user : "+this.bucketName);
        iamManager.createLoginProfile(new CreateLoginProfileRequest().withUserName(this.bucketName).withPassword(this.passwordIAM));
        //create access key to make secure REST to the AWS S3 service API
		Thread.sleep(500);
		System.out.println("Create access key for the user : "+this.bucketName);
        accessKey = iamManager.createAccessKey(new CreateAccessKeyRequest().withUserName(this.usernameIAM)).getAccessKey();
        
        System.out.println("Put user policy to the user : "+this.bucketName);
	    iamManager.putUserPolicy(new PutUserPolicyRequest().withUserName(this.usernameIAM).withPolicyDocument(policy.toJson()).withPolicyName(this.bucketName));
	    Thread.sleep(500);
	    System.out.println("Get and store the IAM credentials of the new user : "+this.bucketName);
	    BasicAWSCredentials iamCreds = new BasicAWSCredentials(this.accessKey.getAccessKeyId(), this.accessKey.getSecretAccessKey());
	    this.s3ClientUser = new AmazonS3Client(iamCreds);

	}
	
	private void deleteUserIfExists(String username) throws Exception
	{
		System.out.println("Try to delete a user with the same name of the user to create");
    	List<User> users = this.iamManager.listUsers(new ListUsersRequest()).getUsers();
    	Integer dimList = users.size();
    	boolean finded = false;
    	Integer index = 0;
    	//look for the user with the name of the bucketName inside the users' list
    	while (index < dimList && finded == false ) {
    		String currUser = users.get(index).getUserName();
    		if (currUser.compareTo(this.bucketName) == 0 ){
    			finded = true;
    			//delete all the inline policy related to the user
				List<String> policyNames = this.iamManager.listUserPolicies(new ListUserPoliciesRequest().withUserName(username)).getPolicyNames();
				for (String policyName : policyNames){
					System.out.println("Delete the user policy : "+policyName+" related to the user : "+this.bucketName);
    				iamManager.deleteUserPolicy(new DeleteUserPolicyRequest().withUserName(username).withPolicyName(policyName));
    			}
				
				//delete login profile
				try{
					System.out.println("Delete the login profile related to the user :"+this.bucketName);
					this.iamManager.deleteLoginProfile(new DeleteLoginProfileRequest().withUserName(username));
				}catch(NoSuchEntityException nsee){
					System.out.println("There isn't a login profile associated to the user");
				}
				//delete Access Key
				List<AccessKeyMetadata> accessKeyMetadataList = this.iamManager.listAccessKeys(new ListAccessKeysRequest().withUserName(username)).getAccessKeyMetadata();

				for (AccessKeyMetadata accessKeyElement : accessKeyMetadataList){
					System.out.println("Delete the access key of the user : "+this.bucketName);
					this.iamManager.deleteAccessKey(new DeleteAccessKeyRequest().withUserName(username).withAccessKeyId(accessKeyElement.getAccessKeyId()));
				}
    			//delete the user
				System.out.println("Delete the user :"+this.bucketName);
    			this.iamManager.deleteUser(new DeleteUserRequest().withUserName(this.bucketName));
    		}
    		index++;
    	}
		
	}
	
	private void deleteRoleIfExists(String roleName) throws Exception{

        iamManager.listRoles();
        
    	List<Role> roles = this.iamManager.listRoles().getRoles();
    	Integer dimList = roles.size();
    	boolean finded = false;
    	Integer index = 0;
    	while (index < dimList && finded == false ) {
    		String currRole = roles.get(index).getRoleName();
    		if (currRole.compareTo("role"+this.bucketName) == 0 ){
    			finded = true;
    			//delete all the inline policies related to the role	
    			List<String> policyNames = this.iamManager.listRolePolicies(new ListRolePoliciesRequest().withRoleName(roleName)).getPolicyNames();
    			for (String element : policyNames){
    				System.out.println("Delete role policy : "+element+" related to the user "+this.bucketName);
    				iamManager.deleteRolePolicy(new DeleteRolePolicyRequest().withRoleName(roleName).withPolicyName(element));
    			}
    			//delete the role 
    			System.out.println("Delete role : "+roleName);
    			iamManager.deleteRole(new DeleteRoleRequest().withRoleName(roleName));
    		}
    		index++;
    	}

	}
	
	//find the bucket and his redundant to delete both them
	public boolean deleteBucketS3(String bucketName){
		//verify if the bucket has a redundant one
        //get the configuration related to replication
		try {
			//delete the user created to manage the bucket 
			this.deleteUserIfExists(bucketName);
			
	        //delete bucket name and redundant bucket if it exist
			if (!this.emptyAndDeleteBucketS3(bucketName)) {
				System.out.println("Error occured on removing the bucket");
				return false;
			}
			if (!this.emptyAndDeleteBucketS3(bucketName+"replica")){
				System.out.println("It wasn't removed the bucket replica maybe it doesn't exist");
			}
			
		}catch( Exception e){
			e.printStackTrace();
			return false;
		}
		
		return true;
		
	}
	
	
	public boolean emptyAndDeleteBucketS3(String bucketName){
		
		try {
			System.out.println("Deleting S3 bucket: " + bucketName);
			ObjectListing objectListing = this.s3ClientMgr.listObjects(bucketName);
   
			while (true) {
				for ( Iterator<?> iterator = objectListing.getObjectSummaries().iterator(); iterator.hasNext(); ) {
					S3ObjectSummary objectSummary = (S3ObjectSummary) iterator.next();
					this.s3ClientMgr.deleteObject(bucketName, objectSummary.getKey());
				}
   
				if (objectListing.isTruncated()) {
					objectListing = this.s3ClientMgr.listNextBatchOfObjects(objectListing);
				} else {
					break;
				}
			};
			VersionListing list = this.s3ClientMgr.listVersions(new ListVersionsRequest().withBucketName(bucketName));
			for ( Iterator<?> iterator = list.getVersionSummaries().iterator(); iterator.hasNext(); ) {
				S3VersionSummary s = (S3VersionSummary)iterator.next();
				this.s3ClientMgr.deleteVersion(bucketName, s.getKey(), s.getVersionId());
			}
			this.s3ClientMgr.deleteBucket(bucketName);

		} catch (AmazonServiceException ase) {
			System.out.println("Caught an AmazonServiceException, which means your request made it " +
					"to Amazon S3, but was rejected with an error response for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
			return false;
		} catch (AmazonClientException ace) {
			System.out.println("Caught an AmazonClientException, which means the client encountered " +
					"an internal error while trying to communicate with S3, such as not being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
			return false;
		}
		return true;
	}
	
	

}

