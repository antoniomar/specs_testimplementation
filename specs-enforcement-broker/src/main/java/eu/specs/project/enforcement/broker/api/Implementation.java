/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specs.project.enforcement.broker.api;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.google.gson.Gson;

import eu.specs.project.enforcement.broker.ChefService;
import eu.specs.project.enforcement.broker.CloudService;
import eu.specs.project.enforcement.broker.CloudServiceOpenStack;
import eu.specs.project.enforcement.broker.ClusterNode;
import eu.specs.project.enforcement.broker.InfoManager;
import eu.specs.project.enforcement.broker.StorageService;
import eu.specs.project.enforcement.broker.StorageServiceS3;
import eu.specs.project.enforcement.broker.entities.BrokerTemplateBean;
import eu.specs.project.enforcement.broker.entities.ImplementationInfo;
import eu.specs.project.enforcement.broker.entities.InstanceDescriptor;
import eu.specs.project.enforcement.broker.entities.MetricInfo;
import eu.specs.project.enforcement.broker.entities.NodesInfo;
import eu.specs.project.enforcement.broker.entities.ImplementationInfo.Status;
import eu.specs.project.enforcement.broker.entities.credentials.NodeCredential;
import eu.specs.project.enforcement.broker.plan.Context;
import eu.specs.project.enforcement.broker.plan.IaaS;
import eu.specs.project.enforcement.broker.plan.Nodes;
import eu.specs.project.enforcement.broker.plan.Plan;
import eu.specs.project.enforcement.broker.plan.Recipes;
import eu.specs.project.enforcement.broker.plan.VMs;


public class Implementation {

	private static Map <String,ImplementationInfo> implementedSlaInfo = new HashMap<String, ImplementationInfo>();
	private static boolean  debug=true;
	
	
	private static String resolveCookbookName (String capability) {
		
		switch (capability){
			case "OSSEC": return "specs-monitoring-ossec";
			case "WEBPOOL": return "specs-enforcement-webpool";
			case "OPENVAS": return "specs-monitoring-openvas";
		}
		
		return "";
	}
 
	private static String resolveOperationCode (String code) {
		

		switch (code){
			case "eq": return "=";
			case "gt": return ">";
			case "ge": return ">=";
			case "lt": return "<";
			case "le": return "<=";
		}
		
		return "";
	}
	
	private static String resolveMetric (String metric) {
		

		if (metric.contains("M13"))
				return "specs.eu/metrics/M13_vulnerability_report_max_age";
	 
		if (metric.contains("M14"))
				return "specs.eu/metrics/M14_vulnerability_list_max_age";
		
		if (metric.contains("M16"))
					return "specs.eu/metrics/M16_dos_report_max_age";
		
		if (metric.contains("M1"))
			return "specs.eu/metrics/M1_redundancy";
		
		if (metric.contains("M2"))
			return "specs.eu/metrics/M2_diversity";
						
						
		return "";
	}
	
	/**
	 * Create all nodes and execute all the recipes based on the slaOffer document
	 * @param slaID identifier of the SLA to implement
	 * @param slaOffer SLA document to implement
	 * @return
	 */
	public static String startImplementation(String slaID, String slaOffer) {
		
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		Document doc = null;
		
		ImplementationInfo info = new ImplementationInfo(slaID);
		
		try {
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(new InputSource(new ByteArrayInputStream(slaOffer.getBytes("utf-8"))));
		} catch (Exception e){
			
			//TODO nothing.. ?
			e.printStackTrace();
		}
		
		doc.getDocumentElement().normalize();
		NodeList gTerms = doc.getElementsByTagName("wsag:GuaranteeTerm"); 
		
		final String WEBPOOL ="WEBPOOL", SVA ="SVA", OPENVAS ="OPENVAS", OSSEC ="OSSEC";
		final String M1 = "specs_webpool_M1", M2 = "specs_webpool_M2", 
							M13_SVA = "specs_sva_M13", M14_SVA = "specs_sva_M14",
							M13_OPENVAS = "specs_openvas_M13",M14_OPENVAS = "specs_openvas_M14",
							M16 = "specs_ossec_M16";
		 
		
		
		Set <String> capabilities = new HashSet<String> ();
		Map <String,String> metrics = new HashMap<String,String>();
		
		XPathFactory xpf = XPathFactory.newInstance();
        XPath xpath = xpf.newXPath();
       
        
        xpath.setNamespaceContext(new NamespaceContext() {
            public String getNamespaceURI(String prefix) {
                if (prefix == null) throw new NullPointerException("Null prefix");
                else if ("specs".equals(prefix)) return "http://specs-project.eu/schemas/sla_related";
                else if ("wsag".equals(prefix)) return "http://schemas.ggf.org/graap/2007/03/ws-agreement";
                else if ("xml".equals(prefix)) return XMLConstants.XML_NS_URI;
                return XMLConstants.NULL_NS_URI;
            }

            // This method isn't necessary for XPath processing.
            public String getPrefix(String uri) {
                throw new UnsupportedOperationException();
            }

            // This method isn't necessary for XPath processing either.
            public Iterator<?> getPrefixes(String uri) {
                throw new UnsupportedOperationException();
            }
        });
        
              
  
		for (int i = 0; i < gTerms.getLength(); i++) {
			
			Node gterm = gTerms.item(i);
					
			String capabilityID="";
			String capabilityName = "";
			try {
				capabilityID = ((Node)xpath.evaluate(gterm.getAttributes().getNamedItem("wsag:Name").getNodeValue(),new InputSource(new ByteArrayInputStream(slaOffer.getBytes("utf-8"))), XPathConstants.NODE)).getAttributes().getNamedItem("id").getNodeValue();
				capabilityName = ((Node)xpath.evaluate(gterm.getAttributes().getNamedItem("wsag:Name").getNodeValue(),new InputSource(new ByteArrayInputStream(slaOffer.getBytes("utf-8"))), XPathConstants.NODE)).getAttributes().getNamedItem("name").getNodeValue();
			} catch (DOMException | XPathExpressionException | UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			NodeList slos = ((Element)gterm).getElementsByTagName("specs:SLO");
			
			List<MetricInfo> metricsInfo = new ArrayList<MetricInfo>();
			
			for (int j = 0; j < slos.getLength(); j++) {
				
				Node slo = slos.item(j);
				
				MetricInfo mInfo = new MetricInfo();
				
				String key = mInfo.id= slo.getAttributes().getNamedItem("name").getNodeValue();
				mInfo.name= slo.getAttributes().getNamedItem("metric-name").getNodeValue();
				String value =((Element)slo).getElementsByTagName("specs:expression").item(0).getTextContent();
				String operator = ((Element)slo).getElementsByTagName("specs:expression").item(0).getAttributes().getNamedItem("op").getNodeValue();
				
				mInfo.agreed = resolveOperationCode(operator) + " " + value; 
				
				metrics.put(key,value);
				metricsInfo.add(mInfo);
				mInfo.cookbook =resolveCookbookName(capabilityID);
				mInfo.metricID=resolveMetric( mInfo.id);
			}
					
			info.metrics.put(capabilityName, metricsInfo);
			capabilities.add(capabilityID);	  
		}
		
		
		
		List<List<String>> recipes=new ArrayList<List<String>> ();
		
		
		int machines = 1;
		int diversity =1;
		
		if (capabilities.contains(WEBPOOL)) {
		
			if (metrics.containsKey(M1)) 
				try {
					machines = Integer.valueOf(metrics.get(M1))+1;
				
				} catch (NumberFormatException e) {
					 	machines = 2;
				}
			else
				machines = 2;
			
			if (metrics.containsKey(M2)){
				
				try {
					diversity = Integer.valueOf(metrics.get(M2));
					
					if (diversity < 1   || diversity > machines -1  )
						diversity = 1;
					else if (diversity > 2)
						diversity=2;
						
				} catch (NumberFormatException e) {
					diversity = 1;
				}
			}
			
		}
		
		int modulo=0;
		
		for (int i  = 0 ; i < machines ; i++){
			
			List <String> temp = new ArrayList<String>();
			
			if(capabilities.contains(WEBPOOL)){
				
				if (i == 0) {
					temp.add("specs-enforcement-webpool::haproxy"); 
				} else {
					
					if (modulo ==0){
						temp.add("specs-enforcement-webpool::apache");
						modulo = (modulo+1)%diversity;
					
					} else if (modulo ==1) {				
						temp.add("specs-enforcement-webpool::nginx");
						modulo = (modulo+1)%diversity;
					
					}	
				}
			}
			
			
			
			if (capabilities.contains(OSSEC)){
				
				
				if (i == 0) temp.add("specs-monitoring-ossec::server");
				else temp.add("specs-monitoring-ossec::agent");
				
			}
			
		    
			if (capabilities.contains(OPENVAS)){
			
				temp.add("specs-monitoring-openvas::manager");
				
				if (i == 0) {
					temp.add("specs-monitoring-openvas::client"); 
				}

				
			}
			
			if (capabilities.contains(SVA)){
				temp.add("sva::sva");
			}
		
			recipes.add(temp);
		}
		
		
		Integer dimensione = recipes.size();
		String plan = "";
		
		plan += "machines = " + recipes.size() + "\n";
		for (List<String> recipe : recipes){
			for (String node : recipe ){
				plan +=node+ "\n";
			}
			plan +="------------------------------"+ "\n";
			plan += "\n"; 
		}
		
		/*
		 * get the provider information from the SLA document
		 */
		BrokerTemplateBean providerBean = new BrokerTemplateBean();
		
		System.out.println(plan);
		try {
		Node node = doc.getElementsByTagName("specs:resources_provider").item(0);
		providerBean.id = "aws-s3";
		providerBean.provider = "openstack-nova";
	    providerBean.zone = node.getAttributes().getNamedItem("zone").getNodeValue();
	    
	    node = doc.getElementsByTagName("specs:VM").item(0);
	    providerBean.appliance = node.getAttributes().getNamedItem("appliance").getNodeValue();
	    providerBean.hw = node.getAttributes().getNamedItem("hardware").getNodeValue();
	    providerBean.description = node.getAttributes().getNamedItem("descr").getNodeValue();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		 * get the credentials of the chef server and of the CSP
		 */
		new InfoManager("resources/", "sws_conf.properties");
		
		/*
		 * The commented lines below are used to verify the functions of the Storege Service
		 */
		/*
		StorageService storage = new StorageService("s3", InfoManager.getProviderCredentials(providerBean.id));
		storage.createContainer("testttstst", "us-west-1", "us-west-2", true);
		String testFileString = "test file to load on Amazon S3";
		byte[] dataToStore = testFileString.getBytes();
				
		storage.uploadBlob("testttstst", "testFile", dataToStore );
		storage.createDirectory("testttstst", "dirtest");
		storage.uploadBlob("testttstst", "dirtest/testFile", dataToStore );
		byte[] test = storage.downloadBlob("testttstst", "testFile");
		String str = new String(test, StandardCharsets.UTF_8);
		System.out.println(str);
		storage.deleteContainer("testttstst");
		*/
		
		createMachines ( slaID,  providerBean,	 InfoManager.getChefServerUsername(),  InfoManager.getChefOrganizationPK(), 
						InfoManager.getChefEndpoint(), InfoManager.getChefUserUsername(), InfoManager.getChefUserPK(), recipes, 
						InfoManager.getHecaIP(), InfoManager.getHecaPort(),info);
		
		return plan;
	}
	
//	public static String createStorageService(String slaID, BrokerTemplateBean bean,
//			String organization, String organizationPK, String chefServerEndpoint,String username, String passwordPK, List<List<String>> recipes,
//			String monitoring_core_ip,String monitoring_core_port, ImplementationInfo extraInfo) {
//
//		ImplementationInfo info = new ImplementationInfo(slaID);
//		info.setMetrics(extraInfo.getMetrics());
//		implementedSlaInfo.put(slaID, info);
//
//		info.instances=recipes.size();
//		info.status=Status.Starting_Implementation;
//			
//		if(debug==true){
//		java.util.Date date= new java.util.Date();		
//		System.out.println(new Timestamp(date.getTime())+" | init impl Secure Storage");
//		}
//		
//		String group = "sla-"+slaID;
//		NodeCredential nodecred = InfoManager.getNodeCredentials("def");
//		
//		//---------- To Acquire VMs --------------
//				//object that give the possibility to generate nodes, execute scripts and bootstrap Chef clients
//				CloudService cloudservice = new CloudService(bean.id, "root", InfoManager.getProviderCredentials(bean.id) );
//				
//				if(debug==true){
//				java.util.Date date= new java.util.Date();	
//				System.out.println(new Timestamp(date.getTime())+" | VMs creating");	
//				}
//				
//				//describe the instance of the node that we want to create 
//				//InstanceDescriptor descr = new InstanceDescriptor(bean.appliance, bean.zone, bean.hw);
//				
////				NodesInfo nodesInfo = cloudservice.createNodesInGroup(group,recipes.size(), descr, 
////						InfoManager.getNodeCredentials("def"),22,80,11211, 9390,1514);
//				/*
//				  with openvas
//				 
//				  NodesInfo nodesInfo = cloudservice.createNodesInGroup(group,recipes.size(), bean.appliance, bean.zone, bean.hw, 
//						nodecred.getPublickey(),nodecred.getPrivatekey(), nodecred.getRootpassword(),22,80,11211,9390);
//				*/
//				
//				if(debug==true){
//				java.util.Date date= new java.util.Date();	
//				System.out.println(new Timestamp(date.getTime())+" | VMs created");
//				}
//				//-----------------------------------------	
//				
//				//-------Update Implementation-Info with nodes info----------------
//				info.status=Status.VMs_Acquired;
//				List<ClusterNode> nodes=nodesInfo.getNodes();
//					
//				for(ClusterNode node : nodes){
//											
//					String s=node.getPrivateIP();
//					info.private_IP_Address.add(s);
//					info.public_IP_Address.add(node.getPublicIP());
//							
//				}
//				//---------------------------------------------------------
//		
//		
//		return "";
//	}
	
	/**
	 * Create all the nodes on the Cloud Service Provider, and execute all the recipes on the nodes 
	 * @param slaID sla identifier
	 * @param bean information about the machines to instantiate: provider, description , zone, hardware, appliance 
	 * @param organization identifier of the organization "ex. cerict"
	 * @param organizationPK organization private key "RSA PRIVATE KEY"
	 * @param chefServerEndpoint the URI of the Chef Server 
	 * @param username Chef Server username
	 * @param passwordPK Chef Server password "RSA PRIVATE KEY"
	 * @param recipes list of recipes that Chefs have to run on the nodes
	 * @param monitoring_core_ip 
	 * @param monitoring_core_port
	 * @param extraInfo It will contain information about the operation state
	 */
	private static void createMachines (String slaID, BrokerTemplateBean bean,
			String organization, String organizationPK, String chefServerEndpoint,String username, String passwordPK, List<List<String>> recipes,
			String monitoring_core_ip,String monitoring_core_port, ImplementationInfo extraInfo) {
        
		try {
			
		
			ImplementationInfo info = new ImplementationInfo(slaID);
			info.setMetrics(extraInfo.getMetrics());
			implementedSlaInfo.put(slaID, info);
	
			info.instances=recipes.size();
			info.status=Status.Starting_Implementation;
				
			if(debug==true){
			java.util.Date date= new java.util.Date();		
			System.out.println(new Timestamp(date.getTime())+" | init impl ");
			}
			
			String group = "sla-"+slaID;
			NodeCredential nodecred = InfoManager.getNodeCredentials("def");
	
			
			//---------- To Acquire VMs --------------
			//object that give the possibility to generate nodes, execute scripts and bootstrap Chef clients
			CloudService cloudservice = new CloudService(bean.id, "root", InfoManager.getProviderCredentials(bean.id) );	
		
		
			if(debug==true){
				java.util.Date date= new java.util.Date();	
				System.out.println(new Timestamp(date.getTime())+" | VMs creating");	
			}
			
			//describe the instance of the node that we want to create 
			InstanceDescriptor descr = new InstanceDescriptor(bean.appliance, bean.zone, bean.hw);

			NodesInfo nodesInfo = cloudservice.createNodesInGroup(group,recipes.size(), descr, 
			InfoManager.getNodeCredentials("def"),22,80,11211, 9390,1514);
//		/*
//		  with openvas
//		 
//		  NodesInfo nodesInfo = cloudservice.createNodesInGroup(group,recipes.size(), bean.appliance, bean.zone, bean.hw, 
//				nodecred.getPublickey(),nodecred.getPrivatekey(), nodecred.getRootpassword(),22,80,11211,9390);
//		*/
//		
//		if(debug==true){
//		java.util.Date date= new java.util.Date();	
//		System.out.println(new Timestamp(date.getTime())+" | VMs created");
//		}
//		//-----------------------------------------	
//		
//		//-------Update Implementation-Info with nodes info----------------
//		info.status=Status.VMs_Acquired;
//		List<ClusterNode> nodes=nodesInfo.getNodes();
//			
//		for(ClusterNode node : nodes){
//									
//			String s=node.getPrivateIP();
//			info.private_IP_Address.add(s);
//			info.public_IP_Address.add(node.getPublicIP());
//					
//		}
//		//---------------------------------------------------------
//		
//		//-----------Preparing VMs : install curl--------------------
//		
//		if(debug==true){
//			java.util.Date date= new java.util.Date();	
//		System.out.println(new Timestamp(date.getTime())+" | VMs preparing");
//		}
//		
//		//create multiple threads that install curl on each node by executing the Script "prepareEnvironmentScript("")"
//		Thread[] threads = new Thread[nodes.size()];
//		for (int i = 0; i < threads.length; i++) {
//		threads[i] = cloudservice.new ExecuteIstructionsOnNode("root",nodes.get(i),prepareEnvironmentScript(""),nodecred.getPrivatekey(),false,cloudservice);
//		threads[i].start();
//		}
//		
//		//waith the termination of all the parallel thread
//		for (int i = 0; i < threads.length; i++) {
//		try {
//		threads[i].join();
//		} catch (InterruptedException ignore) {}
//		}
//		
//		if(debug==true){
//			java.util.Date date= new java.util.Date();	
//		System.out.println(new Timestamp(date.getTime())+" | VMs prepared");
//		}
//		
//		info.status=Status.VMs_Prepared;
//		//------------------------------------------------------------------
//		
//		
//		//------------Bootstrap nodes on chef ----------------------------	
//		ChefService chefService = new  ChefService(organization, organizationPK, chefServerEndpoint, username, passwordPK);
//		
//		//create attributes json
//		
//		/***********************************************************************************************/
//		Plan plan = new Plan();
//		VMs vms = new VMs();
//		//create an array of Nodes where will be stored all the nodes information 
//		ArrayList<Nodes> myNodes = new ArrayList<Nodes>();
//		
//		for (int kk=0; kk<recipes.size();kk++){
//			Nodes node = new Nodes();
//			node.setNode_id(createIDString()); //random
//			ArrayList<Recipes> myRecipes = new ArrayList<Recipes>();
//		
//			for (String myStringRecipe : recipes.get(kk)){
//				Recipes recipeObj = new Recipes();
//				String[] splitted = myStringRecipe.split("::");
//				recipeObj.setCookbook(splitted[0]);
//				recipeObj.setName(splitted[1]);
//				myRecipes.add(recipeObj);				
//			}
//			node.setRecipes(myRecipes);
//			node.setPublic_ip(nodesInfo.getNodes().get(kk).getPublicIP());
//			
//			ArrayList<String> privateIps = new ArrayList<String>();
//			privateIps.add(nodesInfo.getNodes().get(kk).getPrivateIP());
//			node.setPrivate_ips(privateIps);
//			myNodes.add(node);
//		}
//		
//		//set the list of the nodes inside the vms object
//		vms.setNodes(myNodes);
//		
//		IaaS iaas = new IaaS();
//		iaas.setVMs(vms);
//		
//		
//
//		plan.setIaaS(iaas);
//		
//		Context context = new Context();
//		context.setMonitoring_core_ip(monitoring_core_ip);
//		context.setMonitoring_core_port(monitoring_core_port);
//		context.setSlaID(slaID);
//		context.setPlanID(createIDString()); // tra 1 e X
//		
//		
//		
//		
//		plan.setContext(context);
//		
//		System.out.println(new Gson().toJson(plan));
//		
//		String databagItemID = createIDString();
//		
//		chefService.uploadDatabagItem("implementation_plans", databagItemID , new Gson().toJson(plan));
///***********************************************************************************************/
//		
//		
//		String attribute= "{\"implementation_plan_id\":\""+databagItemID+"\"}";		
//			   
//		if(debug==true){
//			java.util.Date date= new java.util.Date();	   
//		System.out.println(new Timestamp(date.getTime())+" | chef node bootstrapping");
//		}
//		
//		chefService.bootstrapChef(group, nodesInfo, cloudservice,attribute);
//		
//		if(debug==true){
//			java.util.Date date= new java.util.Date();	
//		System.out.println(new Timestamp(date.getTime())+" | chef node bootstrapped");
//		}	   
//		info.status=Status.Chef_Node_Bootstrapped;
//
//		for(ClusterNode node : nodes)  info.chefNodesName.add(group+"-"+node.getPrivateIP());
//		//-------------------------------------------------------------------------------------
//		
//		
//		
//		//---------------------Executing recipes on nodes ------------------------------------------
//		for(int i=0;i<nodes.size();i++ ){
//						
//			if(debug==true){
//				java.util.Date date= new java.util.Date();	
//			    System.out.println(new Timestamp(date.getTime())+" | executing recipes: "+Arrays.toString(recipes.get(i).toArray())+" on node "+group+"-"+nodes.get(i).getPrivateIP()+" publicIP:"+nodes.get(i).getPublicIP());				
//			}
//			    threads[i] = chefService.new ExecuteRecipesOnNode(nodes.get(i), recipes.get(i), group,nodecred.getPrivatekey(), cloudservice);
//				threads[i].start();
//							
//				info.recipes.add(InfoRecipes(recipes.get(i)));
//					
//		}
//		
//		for(int i=0;i<nodes.size();i++ ){
//			try {
//			threads[i].join();
//			} catch (InterruptedException ignore) {}
//			}
//		
//		if(debug==true){
//			java.util.Date date= new java.util.Date();		
//		    System.out.println(new Timestamp(date.getTime())+" | recipes completed");
//		}
//		
//		    info.status=Status.Recipes_completed;
//		
//		//-------------------------------------------------------------------
//		
//		
//		    
//		 if(debug==true){
//				java.util.Date date= new java.util.Date();	
//		        System.out.println(new Timestamp(date.getTime())+" | fine impl");
//		 }
//		 
//		info.status= Status.Ready;
//		
		} catch (Exception excp ){
			//TODO check
			excp.printStackTrace();

		}


	}




	private static String[] prepareEnvironmentScript(String appliance) {
		//TODO
		String[] script= {"zypper -n --gpg-auto-import-keys in curl" ,
				"curl -O http://curl.haxx.se/ca/cacert.pem",
				"mv cacert.pem /etc/ca-certificates/",
				"touch .bashrc",
				"echo 'export CURL_CA_BUNDLE=/etc/ca-certificates/cacert.pem' > .bashrc"
		};
		return script;
	}




	public static ImplementationInfo getInfo(String slaid) {
		 
		return implementedSlaInfo.get(slaid);
	}


	private static List<String> InfoRecipes(List<String> recipes){
		
		List<String> ls=new ArrayList<String>();
		for (String s : recipes){
			
			ls.add(s.replace("::","-"));
		}
		
		return ls;
	}

	private static String createIDString(){
	   
	    return  UUID.randomUUID().toString().replace("-","");
			
	}
	
	public BrokerTemplateBean getProviderBean(String providerName){
		//BROKER PROVIDER CREDENTIALS
		//property: broker.provider_credentials.dir
		//servlet context attribute: not in servlet context	
		
		
		List <BrokerTemplateBean> brokertemplates = new ArrayList<BrokerTemplateBean>();
		try{
			URL brokerTemplatesDirectoryUrl = Thread.currentThread().getContextClassLoader().getResource("resources/broker_templates/");
			File brokerTemplatesDirectory = new File(brokerTemplatesDirectoryUrl.toURI());
			int counter = 0;

			
		for ( File f : brokerTemplatesDirectory.listFiles())  {
 				
			 
				byte[] encoded = Files.readAllBytes(Paths.get(f.getAbsolutePath()));			

				BrokerTemplateBean template = BrokerTemplateBean.create(new String(encoded));
				template.id=String.valueOf(counter);
				brokertemplates.add(template);
				counter++;
				int i =0;
				while (i<brokertemplates.size()) {
					if (brokertemplates.get(i).provider == providerName){
						return brokertemplates.get(i);
					}
					i++;
				}
				throw new Exception("there is not a provider that satisfy the required parameters");
	 		}
		}catch(Exception e){
			e.printStackTrace();
		}

		return null;
		
		
		
	}
	
	
	
	
}





